import { Module } from '@nestjs/common';
import { TransferInternalModule } from '@app/internal/transfer/transfer.internal.module';
import { RouterModule } from '@nestjs/core';

@Module({
  imports: [
    TransferInternalModule,
    RouterModule.register([
      {
        path: '/internal',
        children: [
          {
            path: 'transfer',
            module: TransferInternalModule,
          },
        ],
      },
    ]),
  ],
})
export class InternalModule {}
