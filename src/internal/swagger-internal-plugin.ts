import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { TransferInternalModule } from '@app/internal/transfer/transfer.internal.module';

export function swaggerInternalPlugin(app: INestApplication): void {
  const config = new DocumentBuilder()
    .setTitle('LDS Token Internal Api')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config, {
    include: [TransferInternalModule],
  });
  SwaggerModule.setup('/api/ledius-token/internal/docs', app, document, {
    customSiteTitle: 'Ledius Token Internal',
  });
}
