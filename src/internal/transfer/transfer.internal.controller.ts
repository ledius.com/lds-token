import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { TransferHandler } from '@app/transfer/command/transfer.handler';
import { TransferCommand } from '@app/transfer/command/transfer.command';
import { ApiForbiddenResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { TransferResponse } from '@app/transfer/controller/transfer.response';
import { BaseExceptionResponse } from '@app/common/base-exception';

@Controller()
@ApiTags('Transfer')
export class TransferInternalController {
  constructor(private readonly transferHandler: TransferHandler) {}

  @Post()
  @ApiOkResponse({
    type: TransferResponse,
  })
  @HttpCode(200)
  @ApiForbiddenResponse({
    type: BaseExceptionResponse,
  })
  public async transfer(
    @Body() command: TransferCommand,
  ): Promise<TransferResponse> {
    return new TransferResponse(await this.transferHandler.execute(command));
  }
}
