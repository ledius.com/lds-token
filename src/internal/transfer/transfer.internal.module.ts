import { Module } from '@nestjs/common';
import { TransferModule } from '@app/transfer/transfer.module';
import { TransferInternalController } from '@app/internal/transfer/transfer.internal.controller';

@Module({
  imports: [TransferModule],
  controllers: [TransferInternalController],
})
export class TransferInternalModule {}
