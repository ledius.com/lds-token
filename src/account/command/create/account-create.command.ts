import { IsDefined, IsEnum, IsString } from 'class-validator';
import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { v4 } from 'uuid';
import { AccountTypeEnum } from '@app/account/dao/account-type.enum';

export class AccountCreateCommand implements ICommand {
  @IsDefined()
  @IsString()
  @ApiProperty({
    example: v4(),
  })
  public userId: string;

  @IsDefined()
  @IsEnum(AccountTypeEnum)
  public type: AccountTypeEnum = AccountTypeEnum.MAIN;
}
