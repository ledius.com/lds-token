import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AccountCreateCommand } from './account-create.command';
import { Account } from '@app/account/dao/entity/account';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { BinanceCoinContract } from '@app/smart-chain/contracts/binance-coin.contract';
import { v4 } from 'uuid';
import { AccountTypeEnum } from '@app/account/dao/account-type.enum';

@CommandHandler(AccountCreateCommand)
export class AccountCreateHandler
  implements ICommandHandler<AccountCreateCommand, Account>
{
  constructor(
    private readonly accountPgRepository: AccountPgRepository,
    private readonly smartChainService: BinanceCoinContract,
  ) {}

  public async execute(command: AccountCreateCommand): Promise<Account> {
    if (command.type === AccountTypeEnum.MAIN) {
      const foundAccount = await this.accountPgRepository.findMainByUserId(
        command.userId,
      );
      if (foundAccount) {
        return foundAccount;
      }
    }

    const smartChainAccount = await this.smartChainService.createAccount();
    const account = Account.create(
      v4(),
      command.userId,
      smartChainAccount.address,
      smartChainAccount.privateKey,
      command.type,
    );
    await this.accountPgRepository.save(account);

    return account;
  }
}
