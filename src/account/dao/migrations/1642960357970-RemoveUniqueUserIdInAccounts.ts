import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveUniqueUserIdInAccounts1642960357970
  implements MigrationInterface
{
  public readonly name = 'RemoveUniqueUserIdInAccounts1642960357970';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "accounts" DROP CONSTRAINT "UQ_3000dad1da61b29953f07476324"`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "accounts" ADD CONSTRAINT "UQ_3000dad1da61b29953f07476324" UNIQUE ("user_id")`,
    );
  }
}
