import { MigrationInterface, QueryRunner } from 'typeorm';

export class AccountAddTypeColumn1641653985715 implements MigrationInterface {
  name = 'AccountAddTypeColumn1641653985715';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "public"."accounts_type_enum" AS ENUM('0', '1')`,
    );
    await queryRunner.query(
      `ALTER TABLE "accounts" ADD "type" "public"."accounts_type_enum" NOT NULL DEFAULT '0'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "accounts" DROP COLUMN "type"`);
    await queryRunner.query(`DROP TYPE "public"."accounts_type_enum"`);
  }
}
