import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateAccountTable1638704496459 implements MigrationInterface {
  public readonly name = 'CreateAccountTable1638704496459';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "accounts" (
            "id" uuid NOT NULL, 
            "user_id" uuid NOT NULL,
            "address" character varying NOT NULL,
            "private_key" character varying NOT NULL,
            CONSTRAINT "UQ_3000dad1da61b29953f07476324" UNIQUE ("user_id"),
            CONSTRAINT "PK_5a7a02c20412299d198e097a8fe" PRIMARY KEY ("id")
      )`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "accounts"`);
  }
}
