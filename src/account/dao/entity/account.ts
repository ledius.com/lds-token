import { Column, Entity, PrimaryColumn } from 'typeorm';
import { AccountInterface } from '@app/smart-chain/interfaces/account.interface';
import { RecipientInterface } from '@app/smart-chain/interfaces/recipient.interface';
import { AccountTypeEnum } from '@app/account/dao/account-type.enum';
import { AccountAddressInterface } from '@app/smart-chain/interfaces/account-address.interface';

const TABLE_NAME = 'accounts';

@Entity(TABLE_NAME)
export class Account
  implements AccountInterface, RecipientInterface, AccountAddressInterface
{
  public static readonly tableName: string = TABLE_NAME;

  @PrimaryColumn({
    type: 'uuid',
  })
  public readonly id: string;

  @Column({
    type: 'uuid',
  })
  public readonly userId: string;

  @Column({
    type: 'varchar',
  })
  public readonly address: string;

  @Column({
    type: 'varchar',
  })
  public readonly privateKey: string;

  @Column({
    enum: AccountTypeEnum,
    type: 'enum',
    nullable: false,
    default: AccountTypeEnum.MAIN,
  })
  public readonly type: AccountTypeEnum;

  private constructor(
    id: string,
    userId: string,
    address: string,
    privateKey: string,
    type: AccountTypeEnum = AccountTypeEnum.MAIN,
  ) {
    this.id = id;
    this.userId = userId;
    this.address = address;
    this.privateKey = privateKey;
    this.type = type;
  }

  public static create(
    id: string,
    userId: string,
    address: string,
    privateKey: string,
    type: AccountTypeEnum,
  ) {
    return new Account(id, userId, address, privateKey, type);
  }
}
