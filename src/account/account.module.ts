import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from '@app/account/dao/entity/account';
import { AccountCreateHandler } from '@app/account/command/create/account-create.handler';
import { CqrsModule } from '@nestjs/cqrs';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { AccountController } from '@app/account/controller/account.controller';
import { AccountListHandler } from '@app/account/query/list/account-list.handler';
import { SmartChainModule } from '@app/smart-chain/smart-chain.module';
import { AccountUserHandler } from '@app/account/query/user/account-user.handler';

@Module({
  imports: [TypeOrmModule.forFeature([Account]), CqrsModule, SmartChainModule],
  controllers: [AccountController],
  providers: [
    AccountCreateHandler,
    AccountListHandler,
    AccountPgRepository,
    AccountUserHandler,
  ],
  exports: [AccountCreateHandler, AccountListHandler, AccountPgRepository],
})
export class AccountModule {}
