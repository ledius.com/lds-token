import { IQuery } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsUUID } from 'class-validator';

export class AccountUserQuery implements IQuery {
  @IsDefined()
  @ApiProperty()
  @IsUUID('4')
  public readonly userId: string;

  constructor(userId: string) {
    this.userId = userId;
  }
}
