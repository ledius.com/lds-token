import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AccountUserQuery } from '@app/account/query/user/account-user.query';
import { Account } from '@app/account/dao/entity/account';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';

@QueryHandler(AccountUserQuery)
export class AccountUserHandler
  implements IQueryHandler<AccountUserQuery, Account>
{
  constructor(private readonly accountPgRepository: AccountPgRepository) {}

  public async execute(query: AccountUserQuery): Promise<Account> {
    return this.accountPgRepository.getByUserId(query.userId);
  }
}
