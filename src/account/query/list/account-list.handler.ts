import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Account } from '@app/account/dao/entity/account';
import { AccountListQuery } from '@app/account/query/list/account-list.query';

@QueryHandler(AccountListQuery)
export class AccountListHandler
  implements IQueryHandler<AccountListHandler, Account[]>
{
  constructor(private readonly accountRepository: AccountPgRepository) {}

  public async execute(): Promise<Account[]> {
    return this.accountRepository.findAll();
  }
}
