import { Account } from '@app/account/dao/entity/account';
import { ApiProperty } from '@nestjs/swagger';

export class AccountResponse {
  @ApiProperty()
  readonly id: string;

  @ApiProperty()
  readonly userId: string;

  @ApiProperty()
  readonly address: string;

  constructor(account: Account) {
    this.id = account.id;
    this.userId = account.userId;
    this.address = account.address;
  }
}
