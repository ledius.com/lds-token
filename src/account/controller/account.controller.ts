import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Account } from '@app/account/dao/entity/account';
import { AccountCreateCommand } from '@app/account/command/create/account-create.command';
import { AccountResponse } from '@app/account/controller/account.response';
import { AccountListQuery } from '@app/account/query/list/account-list.query';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AccountUserQuery } from '@app/account/query/user/account-user.query';
import { AuthPayload } from '@app/jwt/decorator/auth-payload';
import { TokenPayload } from '@app/jwt/interfaces/token-payload.interface';
import { AuthGuard } from '@app/jwt/guard/auth-guard';

@Controller()
@ApiTags('Account')
export class AccountController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post()
  @ApiCreatedResponse({
    type: AccountResponse,
  })
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  public async create(
    @Body() command: AccountCreateCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<AccountResponse> {
    if (command.userId !== tokenPayload.userId) {
      throw new BadRequestException('You can create account only for you');
    }
    const account = await this.commandBus.execute<
      AccountCreateCommand,
      Account
    >(command);
    return new AccountResponse(account);
  }

  @Get()
  @ApiOkResponse({
    type: AccountResponse,
    isArray: true,
  })
  public async list(
    @Query() query: AccountListQuery,
  ): Promise<AccountResponse[]> {
    const accounts = await this.queryBus.execute<AccountListQuery, Account[]>(
      query,
    );
    return accounts.map((account) => new AccountResponse(account));
  }

  @Get(':userId/user')
  @ApiOkResponse({
    type: AccountResponse,
  })
  public async userAccount(
    @Param('userId') userId: string,
  ): Promise<AccountResponse> {
    return new AccountResponse(
      await this.queryBus.execute(new AccountUserQuery(userId)),
    );
  }
}
