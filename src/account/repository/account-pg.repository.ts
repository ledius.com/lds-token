import { Account } from '@app/account/dao/entity/account';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AccountTypeEnum } from '@app/account/dao/account-type.enum';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';

@Injectable()
export class AccountPgRepository {
  constructor(
    @InjectRepository(Account) private readonly repository: Repository<Account>,
  ) {}

  public async save(account: Account): Promise<void> {
    await this.repository.save(account);
  }

  public async findAll(): Promise<Account[]> {
    return this.repository.find();
  }

  public async findOneByPrivateKey(
    privateKey: string,
  ): Promise<Account | undefined> {
    return this.repository.findOne({
      where: {
        privateKey,
      },
    });
  }

  public async getByUserId(userId: string): Promise<Account> {
    return this.repository.findOneOrFail({
      where: {
        userId,
      },
    });
  }

  public async getMainByUserId(userId: string): Promise<Account> {
    const found = await this.repository.findOne({
      where: {
        userId,
        type: AccountTypeEnum.MAIN,
      },
    });

    if (!found) {
      throw new BaseException({
        statusCode: ErrorCodeEnum.ACCOUNT_NOT_FOUND,
        message: 'Main account not found',
      });
    }
    return found;
  }

  public async findMainByUserId(userId: string): Promise<Account | undefined> {
    return await this.repository.findOne({
      where: {
        userId,
        type: AccountTypeEnum.MAIN,
      },
    });
  }

  public async findLendingByUserId(userId: string): Promise<Account[]> {
    return this.repository.find({
      where: {
        userId,
        type: AccountTypeEnum.LENDING,
      },
    });
  }

  public async findByType(type: AccountTypeEnum): Promise<Account[]> {
    return await this.repository.find({
      where: {
        type,
      },
    });
  }
}
