import { SetMetadata } from '@nestjs/common';
import { TokenSymbolEnum } from '@app/smart-chain/token-symbol.enum';

export const SUPPORT_TOKEN_SYMBOL = 'SUPPORT_TOKEN_SYMBOL';

export function SupportTokenSymbol(symbol: TokenSymbolEnum) {
  return SetMetadata('SUPPORT_TOKEN_SYMBOL', symbol);
}
