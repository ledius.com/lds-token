import { Module } from '@nestjs/common';
import { BinanceCoinContract } from '@app/smart-chain/contracts/binance-coin.contract';
import { Web3Service } from '@app/smart-chain/services/web3.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { BSC_NETWORK_URL } from '@app/smart-chain/constants';
import { LediusContract } from '@app/smart-chain/contracts/ledius.contract';
import { ContractFactoryService } from '@app/smart-chain/services/contract-factory.service';
import { DiscoveryModule } from '@nestjs/core';

@Module({
  imports: [ConfigModule, DiscoveryModule],
  providers: [
    LediusContract,
    BinanceCoinContract,
    ContractFactoryService,
    {
      provide: Web3Service,
      useFactory: (config: ConfigService): Web3Service =>
        new Web3Service(config.get(BSC_NETWORK_URL)),
      inject: [ConfigService],
    },
  ],
  exports: [BinanceCoinContract, LediusContract, ContractFactoryService],
})
export class SmartChainModule {}
