export interface AccountInterface {
  readonly address: string;
  readonly privateKey: string;
}
