export interface AccountAddressInterface {
  readonly address: string;
}
