export interface TransactionReceiptInterface {
  readonly from: string;
  readonly to: string;
  readonly blockHash: string;
  readonly transactionHash: string;
}
