import { AccountAddressInterface } from '@app/smart-chain/interfaces/account-address.interface';
import { RecipientInterface } from '@app/smart-chain/interfaces/recipient.interface';
import { TransactionReceiptInterface } from '@app/smart-chain/interfaces/transaction-receipt.interface';
import { AccountInterface } from '@app/smart-chain/interfaces/account.interface';

export interface BEP20TokenInterface {
  name(): Promise<string>;
  symbol(): Promise<string>;
  decimals(): Promise<number>;
  totalSupply(): Promise<bigint>;
  balanceOf(account: AccountAddressInterface): Promise<bigint>;
  getOwner(): Promise<AccountAddressInterface | undefined>;
  transfer(
    from: AccountInterface,
    to: RecipientInterface,
    amount: bigint,
  ): Promise<TransactionReceiptInterface>;
}
