/**
 * @link https://bscscan.com/address/0x1e8e6601c73989aa59b576a4184a198257c0aef0
 */
export const LDS_CONTRACT = '0x1e8e6601c73989AA59b576a4184a198257C0aef0';
export const BNB_CONTRACT = '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c';

export const BSC_NETWORK_URL = 'BSC_NETWORK_URL';
