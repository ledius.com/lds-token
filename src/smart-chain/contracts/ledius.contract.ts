import { Web3Service } from '@app/smart-chain/services/web3.service';
import { LDS_CONTRACT_ABI } from '@app/smart-chain/abi/lds-contract.abi';
import { LDS_CONTRACT } from '@app/smart-chain/constants';
import { Contract } from 'web3-eth-contract';
import { AccountInterface } from '@app/smart-chain/interfaces/account.interface';
import { Injectable } from '@nestjs/common';
import { RecipientInterface } from '@app/smart-chain/interfaces/recipient.interface';
import { TransactionReceiptInterface } from '@app/smart-chain/interfaces/transaction-receipt.interface';
import { AccountAddressInterface } from '@app/smart-chain/interfaces/account-address.interface';
import { BEP20TokenInterface } from '@app/smart-chain/BEP20-token.interface';
import { SupportTokenSymbol } from '@app/smart-chain/decorators/support-token-symbol';
import { TokenSymbolEnum } from '@app/smart-chain/token-symbol.enum';

@Injectable()
@SupportTokenSymbol(TokenSymbolEnum.LDS)
export class LediusContract implements BEP20TokenInterface {
  private readonly contract: Contract;

  constructor(private readonly web3Service: Web3Service) {
    this.contract = new this.web3Service.eth.Contract(
      LDS_CONTRACT_ABI,
      LDS_CONTRACT,
    );
  }

  public async name(): Promise<string> {
    return await this.contract.methods.name().call();
  }
  public async symbol(): Promise<string> {
    return await this.contract.methods.symbol().call();
  }
  public async decimals(): Promise<number> {
    return Number(await this.contract.methods.decimals().call());
  }
  public async totalSupply(): Promise<bigint> {
    return BigInt(await this.contract.methods.totalSupply().call());
  }
  public async getOwner(): Promise<AccountAddressInterface> {
    return {
      address: await this.contract.methods.getOwner().call(),
    };
  }

  public async balanceOf(account: AccountAddressInterface): Promise<bigint> {
    return BigInt(
      await this.contract.methods.balanceOf(account.address).call(),
    );
  }

  public async transfer(
    from: AccountInterface,
    to: RecipientInterface,
    amount: bigint,
  ): Promise<TransactionReceiptInterface> {
    this.web3Service.eth.accounts.wallet.add(from);
    const receipt = await this.contract.methods
      .transfer(to.address, amount)
      .send({
        from: from.address,
        gasPrice: await this.web3Service.eth.getGasPrice(),
        gas: await this.web3Service.eth.estimateGas({ from: from.address }),
      });

    return {
      from: receipt.from,
      to: receipt.to,
      blockHash: receipt.blockHash,
      transactionHash: receipt.transactionHash,
    };
  }
}
