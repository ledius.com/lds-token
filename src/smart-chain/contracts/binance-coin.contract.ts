import { Web3Service } from '@app/smart-chain/services/web3.service';
import { Injectable } from '@nestjs/common';
import { AccountInterface } from '@app/smart-chain/interfaces/account.interface';
import { AccountAddressInterface } from '@app/smart-chain/interfaces/account-address.interface';
import { Contract } from 'web3-eth-contract';
import { BNB_CONTRACT } from '@app/smart-chain/constants';
import { BNB_CONTRACT_ABI } from '@app/smart-chain/abi/bnb-contract.abi';
import { BEP20TokenInterface } from '@app/smart-chain/BEP20-token.interface';
import { RecipientInterface } from '@app/smart-chain/interfaces/recipient.interface';
import { TransactionReceiptInterface } from '@app/smart-chain/interfaces/transaction-receipt.interface';
import { SupportTokenSymbol } from '@app/smart-chain/decorators/support-token-symbol';
import { TokenSymbolEnum } from '@app/smart-chain/token-symbol.enum';

@Injectable()
@SupportTokenSymbol(TokenSymbolEnum.BNB)
export class BinanceCoinContract implements BEP20TokenInterface {
  private readonly contract: Contract;

  constructor(private readonly web3Service: Web3Service) {
    this.contract = new this.web3Service.eth.Contract(
      BNB_CONTRACT_ABI,
      BNB_CONTRACT,
    );
  }

  public async createAccount(): Promise<AccountInterface> {
    return this.web3Service.eth.accounts.create();
  }

  public async getAccount(privateKey: string): Promise<AccountInterface> {
    return this.web3Service.eth.accounts.privateKeyToAccount(privateKey);
  }

  public async name(): Promise<string> {
    return 'BNB';
  }
  public async symbol(): Promise<string> {
    return 'BNB';
  }
  public async decimals(): Promise<number> {
    return 18;
  }
  public async totalSupply(): Promise<bigint> {
    return BigInt(0);
  }
  public async getOwner(): Promise<AccountAddressInterface | undefined> {
    return undefined;
  }

  public async balanceOf(account: AccountAddressInterface): Promise<bigint> {
    return BigInt(await this.web3Service.eth.getBalance(account.address));
  }

  public async transfer(
    from: AccountInterface,
    to: RecipientInterface,
    amount: bigint,
  ): Promise<TransactionReceiptInterface> {
    const fromBSCAccount = this.web3Service.eth.accounts.privateKeyToAccount(
      from.privateKey,
    );
    this.web3Service.eth.accounts.wallet.add(from);
    const gasPrice = await this.web3Service.eth.getGasPrice();
    const txId = await this.web3Service.eth.getTransactionCount(from.address);
    const tx = {
      from: from.address,
      to: to.address,
      value: amount.toString(),
      nonce: txId,
    };
    const gasLimit = await this.web3Service.eth.estimateGas(tx);
    const singedTx = await fromBSCAccount.signTransaction({
      ...tx,
      gasPrice,
      gas: gasLimit,
    });

    return this.web3Service.eth.sendSignedTransaction(singedTx.rawTransaction);
  }
}
