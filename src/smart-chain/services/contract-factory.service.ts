import { TokenSymbolEnum } from '@app/smart-chain/token-symbol.enum';
import { BEP20TokenInterface } from '@app/smart-chain/BEP20-token.interface';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';
import { Injectable } from '@nestjs/common';
import { BinanceCoinContract } from '@app/smart-chain/contracts/binance-coin.contract';
import { LediusContract } from '@app/smart-chain/contracts/ledius.contract';
import { DiscoveryService, ModuleRef, Reflector } from '@nestjs/core';
import { SUPPORT_TOKEN_SYMBOL } from '@app/smart-chain/decorators/support-token-symbol';

@Injectable()
export class ContractFactoryService {
  constructor(
    private readonly module: ModuleRef,
    private readonly reflector: Reflector,
    private readonly discoveryService: DiscoveryService,
  ) {}

  public getSupportedSymbol(): TokenSymbolEnum[] {
    const metatypes = this.discoveryService
      .getProviders()
      .map(({ metatype }) => metatype)
      .filter(Boolean);

    return this.reflector.getAllAndMerge(SUPPORT_TOKEN_SYMBOL, metatypes);
  }

  public getContract(symbol: TokenSymbolEnum): BEP20TokenInterface {
    if (symbol === TokenSymbolEnum.BNB) {
      return this.module.get(BinanceCoinContract);
    }

    if (symbol === TokenSymbolEnum.LDS) {
      return this.module.get(LediusContract);
    }

    throw new BaseException({
      message: `Contract for ${symbol} not exists`,
      statusCode: ErrorCodeEnum.NOT_FOUND,
    });
  }
}
