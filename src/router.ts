import { RouterModule } from '@nestjs/core';
import { OfferModule } from '@app/offer/offer.module';
import { AccountModule } from '@app/account/account.module';
import { BankDetailsModule } from '@app/bank-details/bank-details.module';
import { BalanceModule } from '@app/balance/balance.module';
import { TransferModule } from '@app/transfer/transfer.module';
import { LendingModule } from '@app/lending/lending.module';
import { DispenserModule } from '@app/dispenser/dispenser.module';
import { HistoryModule } from '@app/history/history.module';
import { UsersModule } from '@app/users/users.module';

export const Router = RouterModule.register([
  {
    path: '/api/ledius-token',
    children: [
      {
        path: 'offers',
        module: OfferModule,
      },
      {
        path: 'accounts',
        module: AccountModule,
      },
      {
        path: 'banks-details',
        module: BankDetailsModule,
      },
      {
        path: 'balance',
        module: BalanceModule,
      },
      {
        path: 'transfer',
        module: TransferModule,
      },
      {
        path: 'lending',
        module: LendingModule,
      },
      {
        path: 'dispenser-account',
        module: DispenserModule,
      },
      {
        path: 'history',
        module: HistoryModule,
      },
      {
        path: 'users',
        module: UsersModule,
      },
    ],
  },
]);
