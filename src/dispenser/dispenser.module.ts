import {
  ConsoleLogger,
  LoggerService,
  Module,
  OnModuleInit,
} from '@nestjs/common';
import { WALLET_DISPENSER_PRIVATE_KEY } from '@app/lending/constant';
import { ConfigModule, ConfigService } from '@nestjs/config';
import assert from 'assert';
import { DispenseAccountService } from '@app/dispenser/service/dispense-account.service';
import { AccountModule } from '@app/account/account.module';
import { SmartChainModule } from '@app/smart-chain/smart-chain.module';
import { DispenserAccountController } from '@app/dispenser/controller/dispenser-account.controller';
import { DispenseBnbService } from '@app/dispenser/service/dispense-bnb.service';
import { BalanceModule } from '@app/balance/balance.module';

@Module({
  imports: [AccountModule, SmartChainModule, ConfigModule, BalanceModule],
  controllers: [DispenserAccountController],
  providers: [
    DispenseBnbService,
    DispenseAccountService,
    {
      provide: WALLET_DISPENSER_PRIVATE_KEY,
      useFactory: (config: ConfigService): string => {
        const privateKey = config.get(WALLET_DISPENSER_PRIVATE_KEY);
        assert.ok(typeof privateKey === 'string');
        return privateKey;
      },
      inject: [ConfigService],
    },
  ],
  exports: [DispenseAccountService, DispenseBnbService],
})
export class DispenserModule implements OnModuleInit {
  private readonly logger: LoggerService = new ConsoleLogger(
    DispenserModule.name,
  );

  constructor(
    private readonly dispenseAccountService: DispenseAccountService,
  ) {}

  public async onModuleInit(): Promise<void> {
    const account = await this.dispenseAccountService.ensureExistence();
    this.logger.log(`Dispenser account setup ${account.address}`);
  }
}
