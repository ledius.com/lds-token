import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DispenseAccountService } from '@app/dispenser/service/dispense-account.service';
import { AccountResponse } from '@app/account/controller/account.response';
import { BalanceResponse } from '@app/balance/controller/balance.response';
import { AccountBalanceHandler } from '@app/balance/query/account-balance/account-balance.handler';
import { AccountBalanceQuery } from '@app/balance/query/account-balance/account-balance.query';

@Controller()
@ApiTags('Dispenser Account')
export class DispenserAccountController {
  constructor(
    private readonly dispenseAccountService: DispenseAccountService,
    private readonly accountBalanceHandler: AccountBalanceHandler,
  ) {}

  @Get()
  public async getInfo(): Promise<{
    account: AccountResponse;
    balances: BalanceResponse[];
  }> {
    const account = new AccountResponse(
      await this.dispenseAccountService.ensureExistence(),
    );
    const balances = await this.accountBalanceHandler.execute(
      new AccountBalanceQuery(account.address),
    );

    return { account, balances };
  }
}
