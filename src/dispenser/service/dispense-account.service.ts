import { Inject, Injectable } from '@nestjs/common';
import { WALLET_DISPENSER_PRIVATE_KEY } from '@app/lending/constant';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { RecipientInterface } from '@app/smart-chain/interfaces/recipient.interface';
import { LediusContract } from '@app/smart-chain/contracts/ledius.contract';
import { BinanceCoinContract } from '@app/smart-chain/contracts/binance-coin.contract';
import { AccountInterface } from '@app/smart-chain/interfaces/account.interface';
import { TransactionReceiptInterface } from '@app/smart-chain/interfaces/transaction-receipt.interface';
import { Account } from '@app/account/dao/entity/account';
import { AccountTypeEnum } from '@app/account/dao/account-type.enum';

export const DISPENSER_ACCOUNT_ID = 'd5ff481d-a889-4bd4-a9ab-338f483861f6';
export const DISPENSER_ACCOUNT_USER_ID = 'fe2093c2-f5ad-4a9a-8bc8-35139471fe3f';

@Injectable()
export class DispenseAccountService {
  public constructor(
    @Inject(WALLET_DISPENSER_PRIVATE_KEY)
    private readonly walletDispenseAccountPrivateKey: string,
    private readonly accountPgRepository: AccountPgRepository,
    private readonly lediusContract: LediusContract,
    private readonly smartChainService: BinanceCoinContract,
  ) {}

  public async ensureExistence(): Promise<Account> {
    const account = await this.getAccount();
    const found = await this.accountPgRepository.findOneByPrivateKey(
      account.privateKey,
    );

    if (found) {
      return found;
    }

    const dispenserAccount = Account.create(
      DISPENSER_ACCOUNT_ID,
      DISPENSER_ACCOUNT_USER_ID,
      account.address,
      account.privateKey,
      AccountTypeEnum.MAIN,
    );

    await this.accountPgRepository.save(dispenserAccount);

    return dispenserAccount;
  }

  public async getAccount(): Promise<AccountInterface> {
    return this.smartChainService.getAccount(
      this.walletDispenseAccountPrivateKey,
    );
  }

  public async transfer(
    recipient: RecipientInterface,
    amount: bigint,
  ): Promise<TransactionReceiptInterface> {
    const account = await this.getAccount();
    return this.lediusContract.transfer(account, recipient, amount);
  }
}
