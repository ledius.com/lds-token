import { Injectable } from '@nestjs/common';
import { DispenseAccountService } from '@app/dispenser/service/dispense-account.service';
import { ContractFactoryService } from '@app/smart-chain/services/contract-factory.service';
import { RecipientInterface } from '@app/smart-chain/interfaces/recipient.interface';
import { TransactionReceiptInterface } from '@app/smart-chain/interfaces/transaction-receipt.interface';
import { TokenSymbolEnum } from '@app/smart-chain/token-symbol.enum';

@Injectable()
export class DispenseBnbService {
  private readonly feeDispenseAmount: bigint = BigInt(3_000_000_000_000_000);
  private readonly minimalFeePrice: bigint = BigInt(1_000_000_000_000_000);

  constructor(
    private readonly dispenseAccountService: DispenseAccountService,
    private readonly contractFactoryService: ContractFactoryService,
  ) {}

  public async dispenseFee(
    recipient: RecipientInterface,
  ): Promise<TransactionReceiptInterface> {
    const contract = this.contractFactoryService.getContract(
      TokenSymbolEnum.BNB,
    );

    const balanceOfRecipient = await contract.balanceOf(recipient);
    if (balanceOfRecipient >= this.minimalFeePrice) {
      return;
    }

    return contract.transfer(
      await this.dispenseAccountService.getAccount(),
      recipient,
      this.feeDispenseAmount,
    );
  }
}
