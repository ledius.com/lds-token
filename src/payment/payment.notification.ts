import { Type } from 'class-transformer';
import { PaymentStatusEnum } from '@app/payment/payment-status.enum';
import { OrderTypeEnum } from '@app/payment/order-type.enum';
import { ApiProperty } from '@nestjs/swagger';

export interface Data {
  orderType?: OrderTypeEnum;
  userId?: string;
}

export class PaymentNotification<T extends Data = Data> {
  @ApiProperty()
  public TerminalKey!: string;

  @ApiProperty()
  public OrderId!: string;

  @Type(() => Boolean)
  @ApiProperty()
  public Success!: boolean;

  @ApiProperty({
    example: PaymentStatusEnum.CONFIRMED,
  })
  public Status!: PaymentStatusEnum;

  @Type(() => Number)
  @ApiProperty()
  public PaymentId!: number;

  @ApiProperty()
  public ErrorCode!: string;

  @Type(() => Number)
  @ApiProperty()
  public Amount!: number;

  @Type(() => Number)
  @ApiProperty()
  public RebillId!: number;

  @Type(() => Number)
  @ApiProperty()
  public CardId!: number;

  @ApiProperty()
  public Pan!: string;

  @ApiProperty()
  public ExpDate!: string;

  @ApiProperty()
  public Token!: string;

  @ApiProperty()
  public Data!: T;
}
