import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PaymentService } from '@app/payment/service/payment.service';
import { NotificationUrlService } from '@app/payment/service/notification-url.service';

export const PAYMENT_API_URL = 'PAYMENT_API_URL';
export const APP_BASE_URL = 'APP_BASE_URL';

@Module({
  imports: [
    ConfigModule,
    HttpModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        baseURL: config.get(PAYMENT_API_URL),
      }),
    }),
  ],
  providers: [
    PaymentService,
    {
      provide: APP_BASE_URL,
      inject: [ConfigService],
      useFactory: (config: ConfigService): string => config.get(APP_BASE_URL),
    },
    {
      provide: NotificationUrlService,
      useFactory: (appUrl: string): NotificationUrlService =>
        new NotificationUrlService(appUrl),
      inject: [APP_BASE_URL],
    },
  ],
  exports: [PaymentService, NotificationUrlService],
})
export class PaymentModule {}
