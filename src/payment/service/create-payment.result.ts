import { ApiProperty } from '@nestjs/swagger';

export class CreatePaymentResult {
  @ApiProperty()
  public readonly paymentUrl: string;

  constructor(paymentUrl: string) {
    this.paymentUrl = paymentUrl;
  }
}
