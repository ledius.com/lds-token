import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { OrderTypeEnum } from '@app/payment/order-type.enum';
import { lastValueFrom } from 'rxjs';
import { CreatePaymentResult } from '@app/payment/service/create-payment.result';
import { Receipt } from '@app/payment/service/receipt';

export interface CreatePaymentOptions {
  readonly Amount: number;
  readonly OrderId: string;
  readonly CustomerKey: string;
  readonly DATA: {
    orderType: OrderTypeEnum;
  };
  readonly Receipt: Receipt;
  readonly NotificationURL?: string;
  readonly SuccessURL?: string;
  readonly FailURL?: string;
}

@Injectable()
export class PaymentService {
  constructor(private readonly http: HttpService) {}

  public async createPayment(
    options: CreatePaymentOptions,
    accessToken: string,
  ): Promise<CreatePaymentResult> {
    const response = await lastValueFrom(
      this.http.post<CreatePaymentResult>(
        '/api/v1/tinkoff/payment/init',
        options,
        {
          headers: {
            Authorization: `bearer ${accessToken}`,
          },
        },
      ),
    );
    return new CreatePaymentResult(response.data.paymentUrl);
  }
}
