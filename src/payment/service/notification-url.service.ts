import { APP_BASE_URL } from '@app/payment/payment.module';
import { Inject, Injectable } from '@nestjs/common';

@Injectable()
export class NotificationUrlService {
  constructor(@Inject(APP_BASE_URL) private readonly appUrl: string) {}

  public generate(path: string): string {
    const url = new URL(this.appUrl);
    url.pathname = `${url.pathname}${path}`;
    return url.toString();
  }
}
