export enum OrderTypeEnum {
  COURSE = 'COURSE',
  BID = 'BID',
  OFFER = 'OFFER',
}
