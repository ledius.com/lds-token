import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AccountBalanceQuery } from '@app/balance/query/account-balance/account-balance.query';
import { ContractFactoryService } from '@app/smart-chain/services/contract-factory.service';
import { BEP20TokenInterface } from '@app/smart-chain/BEP20-token.interface';

export interface AssetBalance {
  readonly symbol: string;
  readonly balance: string;
  readonly decimals: number;
}

@QueryHandler(AccountBalanceQuery)
export class AccountBalanceHandler
  implements IQueryHandler<AccountBalanceQuery, unknown>
{
  constructor(
    private readonly contractFactoryService: ContractFactoryService,
  ) {}

  public async execute(query: AccountBalanceQuery): Promise<AssetBalance[]> {
    return await Promise.all(
      this.contractFactoryService.getSupportedSymbol().map(async (symbol) => {
        const contract: BEP20TokenInterface =
          this.contractFactoryService.getContract(symbol);

        return {
          balance: String(await contract.balanceOf(query)),
          symbol: symbol,
          decimals: await contract.decimals(),
        };
      }),
    );
  }
}
