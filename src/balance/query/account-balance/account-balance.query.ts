import { IQuery } from '@nestjs/cqrs';
import { IsDefined, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { AccountAddressInterface } from '@app/smart-chain/interfaces/account-address.interface';

export class AccountBalanceQuery implements IQuery, AccountAddressInterface {
  @IsString()
  @IsDefined()
  @ApiProperty()
  public readonly address: string;

  constructor(address: string) {
    this.address = address;
  }
}
