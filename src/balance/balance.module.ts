import { Module } from '@nestjs/common';
import { SmartChainModule } from '@app/smart-chain/smart-chain.module';
import { AccountBalanceHandler } from '@app/balance/query/account-balance/account-balance.handler';
import { BalanceController } from '@app/balance/controller/balance.controller';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [SmartChainModule, CqrsModule],
  controllers: [BalanceController],
  providers: [AccountBalanceHandler],
  exports: [AccountBalanceHandler],
})
export class BalanceModule {}
