import { AssetBalance } from '@app/balance/query/account-balance/account-balance.handler';
import { ApiProperty } from '@nestjs/swagger';

export class BalanceResponse {
  @ApiProperty()
  public readonly symbol: string;

  @ApiProperty()
  public readonly balance: string;

  constructor(assetBalance: AssetBalance) {
    this.symbol = assetBalance.symbol;
    this.balance = assetBalance.balance;
  }
}
