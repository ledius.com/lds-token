import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { QueryBus } from '@nestjs/cqrs';
import { AccountBalanceQuery } from '@app/balance/query/account-balance/account-balance.query';

@Controller()
@ApiTags('Balance')
export class BalanceController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('account')
  public async accountBalance(
    @Query() command: AccountBalanceQuery,
  ): Promise<unknown> {
    return this.queryBus.execute(command);
  }
}
