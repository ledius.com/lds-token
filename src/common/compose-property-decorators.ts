export function composePropertyDecorators(
  ...decorators: PropertyDecorator[]
): PropertyDecorator {
  return (target, propertyKey) => {
    decorators.forEach((decorator) => decorator(target, propertyKey));
  };
}
