import { FindConditions } from 'typeorm';

export function conditionsMapping<
  T,
  O extends Record<string, any> = Record<string, any>,
>(...options: (O | FindConditions<T>)[]): FindConditions<T> {
  const merged = options.reduce(
    (accum, object) => ({ ...accum, ...object }),
    {},
  );
  let where = {} as T;

  for (const [key, value] of Object.entries(merged)) {
    if (key === 'extra') {
      continue;
    }
    if (value !== undefined) {
      where = { ...where, [key]: value };
    }
  }

  return where;
}
