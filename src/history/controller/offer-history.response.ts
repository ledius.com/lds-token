import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OfferStatusEnum } from '@app/offer/dao/offer-status.enum';
import { OfferTypeEnum } from '@app/offer/dao/offer-type.enum';
import { BankDetailsResponse } from '@app/bank-details/controller/bank-details.response';
import { Offer } from '@app/offer/dao/entity/offer';
import { Rate } from '@app/offer/controller/offer.response';
import { AuthUserInterface } from '@app/http-auth/service/auth-client.service';
import { OfferAccountResponse } from '@app/history/controller/offer-account.response';

export class OfferHistoryResponse {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  public readonly amount: string;

  @ApiProperty({
    type: Rate,
  })
  @Type(() => Rate)
  public readonly rate: Rate;

  @ApiProperty({
    type: 'integer',
    enum: OfferStatusEnum,
    enumName: 'OfferStatusEnum',
  })
  public readonly status: OfferStatusEnum;

  @ApiProperty({
    type: 'integer',
    enum: OfferTypeEnum,
    enumName: 'OfferTypeEnum',
  })
  public readonly type: OfferTypeEnum;

  @ApiProperty({
    type: OfferAccountResponse,
  })
  public readonly owner: OfferAccountResponse;

  @ApiProperty({
    type: OfferAccountResponse,
    nullable: true,
  })
  public readonly reservedBy: OfferAccountResponse | null;

  @ApiProperty({
    type: Date,
    nullable: true,
  })
  public readonly reservedAt: Date | null;

  @ApiProperty({
    type: BankDetailsResponse,
    nullable: true,
  })
  @Type(() => BankDetailsResponse)
  public readonly bankDetails: BankDetailsResponse | null;

  @ApiProperty({
    type: Date,
  })
  public readonly publishedAt: Date;

  @ApiProperty({
    type: Date,
  })
  public readonly createdAt: Date;

  @ApiProperty({
    type: Date,
  })
  public readonly updatedAt: Date;

  constructor(
    offer: Offer,
    owner: AuthUserInterface,
    reserved: AuthUserInterface = null,
  ) {
    this.id = offer.id;
    this.owner = new OfferAccountResponse(owner, offer.owner);
    this.amount = offer.amount.toString();
    this.rate = new Rate(offer.rate);
    this.reservedBy = reserved
      ? new OfferAccountResponse(reserved, offer.reservedBy)
      : null;
    this.reservedAt = offer.reservedAt || null;
    this.bankDetails =
      offer.bankDetails && new BankDetailsResponse(offer.bankDetails);
    this.publishedAt = offer.publishedAt;
    this.createdAt = offer.createdAt;
    this.updatedAt = offer.updatedAt;
    this.status = offer.status;
    this.type = offer.type;
  }
}
