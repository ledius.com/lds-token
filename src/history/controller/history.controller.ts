import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { OfferListResponse } from '@app/offer/controller/offer-list.response';
import { ApiPaginated } from '@app/common/pagination/api-paginated';
import { QueryPagination } from '@app/common/pagination/query-pagination';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { QueryBus } from '@nestjs/cqrs';
import { GetHistoryQuery } from '@app/history/cqrs/get-history.query';
import { OfferHistoryResponse } from '@app/history/controller/offer-history.response';
import { OfferHistoryListResponse } from '@app/history/controller/offer-history-list.response';
import { GetOffersQuery } from '@app/history/cqrs/get-offers.query';
import { PaginatedResult } from '@app/common/pagination/paginated-result';
import { Offer } from '@app/offer/dao/entity/offer';
import { AuthClientService } from '@app/http-auth/service/auth-client.service';
import { of } from 'rxjs';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { GetLendingListResult } from '@app/history/cqrs/get-lending-list.result';
import { PaginationResponse } from '@app/common/pagination/pagination.response';
import { GetLendingListQuery } from '@app/history/cqrs/get-lending-list.query';

@Controller()
@ApiTags('History')
export class HistoryController {
  public constructor(
    private readonly queryBus: QueryBus,
    private readonly authClientService: AuthClientService,
    private readonly offerPgRepository: OfferPgRepository,
  ) {}

  @Get()
  @ApiOkResponse({
    type: OfferListResponse,
  })
  @ApiPaginated()
  public async list(
    @Query() query: GetHistoryQuery,
    @QueryPagination() pagination: PaginationDto,
  ): Promise<OfferListResponse> {
    return new OfferListResponse(
      await this.queryBus.execute(query.withPagination(pagination)),
    );
  }

  @Get('offers')
  @ApiOkResponse({
    type: OfferHistoryListResponse,
  })
  @ApiPaginated()
  public async getOffers(
    @Query() query: GetOffersQuery,
    @QueryPagination() paginationQuery: PaginationDto,
  ): Promise<OfferHistoryListResponse> {
    const { items, pagination } = await this.queryBus.execute<
      GetOffersQuery,
      PaginatedResult<Offer>
    >(query.withPagination(paginationQuery));

    const offersResponse = await Promise.all(
      items.map(async (offer) => {
        const owner = await this.authClientService.getUserById(
          offer.owner.userId,
        );
        const reserved = offer.reservedBy
          ? await this.authClientService.getUserById(offer.reservedBy.userId)
          : null;

        return new OfferHistoryResponse(offer, owner, reserved);
      }),
    );

    return new OfferHistoryListResponse({ items: offersResponse, pagination });
  }

  @Get('offers/:offerId')
  @ApiOkResponse({
    type: OfferHistoryResponse,
  })
  public async getOffer(
    @Param('offerId') offerId: string,
  ): Promise<OfferHistoryResponse> {
    const offer = await this.offerPgRepository.getById(offerId);
    const owner = await this.authClientService.getUserById(offer.owner.userId);
    const reserved = offer.reservedBy
      ? await this.authClientService.getUserById(offer.reservedBy.userId)
      : null;

    return new OfferHistoryResponse(offer, owner, reserved);
  }

  @Get('lending')
  @ApiOkResponse()
  @ApiPaginated()
  public async getLendingList(
    @Query() query: GetLendingListQuery,
    @QueryPagination() pagination: PaginationDto,
  ): Promise<PaginatedResult<GetLendingListResult>> {
    query.pagination = pagination;
    const lendingList = await this.queryBus.execute<
      GetLendingListQuery,
      PaginatedResult<GetLendingListResult>
    >(query);

    return {
      items: lendingList.items,
      pagination: lendingList.pagination,
    };
  }
}
