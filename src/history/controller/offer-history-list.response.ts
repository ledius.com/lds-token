import { ApiProperty } from '@nestjs/swagger';
import { PaginationResponse } from '@app/common/pagination/pagination.response';
import { OfferHistoryResponse } from '@app/history/controller/offer-history.response';
import { PaginatedResult } from '@app/common/pagination/paginated-result';

export class OfferHistoryListResponse {
  @ApiProperty({
    type: PaginationResponse,
  })
  public readonly pagination: PaginationResponse;

  @ApiProperty({
    type: OfferHistoryResponse,
    isArray: true,
  })
  public readonly items: OfferHistoryResponse[];

  constructor({ items, pagination }: PaginatedResult<OfferHistoryResponse>) {
    this.pagination = pagination;
    this.items = items;
  }
}
