import { AuthUserInterface } from '@app/http-auth/service/auth-client.service';
import { Account } from '@app/account/dao/entity/account';
import { ApiProperty } from '@nestjs/swagger';

export class OfferAccountResponse {
  @ApiProperty({})
  public readonly name: string;

  @ApiProperty({})
  public readonly email: string;

  @ApiProperty({})
  public readonly userId: string;

  @ApiProperty({})
  public readonly picture: string;

  @ApiProperty({
    type: 'string',
    isArray: true,
  })
  public readonly roles: string[];

  @ApiProperty({})
  public readonly accountId: string;

  @ApiProperty({})
  public readonly accountAddress: string;

  constructor(authUser: AuthUserInterface, account: Account) {
    this.name = authUser.name;
    this.email = authUser.email;
    this.userId = authUser.id;
    this.picture = authUser.picture;
    this.roles = authUser.roles || [];
    this.accountId = account.id;
    this.accountAddress = account.address;
  }
}
