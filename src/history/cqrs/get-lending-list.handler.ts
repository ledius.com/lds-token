import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetLendingListQuery } from '@app/history/cqrs/get-lending-list.query';
import { PaginatedResult } from '@app/common/pagination/paginated-result';
import { Injectable } from '@nestjs/common';
import { LendingPgRepository } from '@app/lending/repository/lending-pg.repository';
import { AuthClientService } from '@app/http-auth/service/auth-client.service';
import { GetLendingListResult } from '@app/history/cqrs/get-lending-list.result';

@Injectable()
@QueryHandler(GetLendingListQuery)
export class GetLendingListHandler
  implements
    IQueryHandler<GetLendingListQuery, PaginatedResult<GetLendingListResult>>
{
  constructor(
    private readonly lendingPgRepository: LendingPgRepository,
    private readonly authClientService: AuthClientService,
  ) {}

  public async execute(
    query: GetLendingListQuery,
  ): Promise<PaginatedResult<GetLendingListResult>> {
    const { items, total } = await this.lendingPgRepository.findBy(
      query,
      query.pagination,
    );

    const lendingList = await Promise.all(
      items.map(async (item) => {
        const user = await this.authClientService.getUserById(
          item.account.userId,
        );
        return new GetLendingListResult(item, user);
      }),
    );

    return {
      items: lendingList,
      pagination: query.pagination.withTotal(total),
    };
  }
}
