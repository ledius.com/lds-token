import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Offer } from '@app/offer/dao/entity/offer';
import { Injectable } from '@nestjs/common';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { GetOffersQuery } from '@app/history/cqrs/get-offers.query';
import { PaginatedResult } from '@app/common/pagination/paginated-result';

@Injectable()
@QueryHandler(GetOffersQuery)
export class GetOffersHandler
  implements IQueryHandler<GetOffersQuery, PaginatedResult<Offer>>
{
  constructor(private readonly offerPgRepository: OfferPgRepository) {}

  public async execute(query: GetOffersQuery): Promise<PaginatedResult<Offer>> {
    const [offers, total] = await this.offerPgRepository.findBy({
      extra: {
        pagination: query.pagination,
      },
    });

    return { items: offers, pagination: query.pagination.withTotal(total) };
  }
}
