import { ApiProperty } from '@nestjs/swagger';
import { Lending } from '@app/lending/dao/entity/lending';
import { AuthUserInterface } from '@app/http-auth/service/auth-client.service';
import { AccountResponse } from '@app/account/controller/account.response';
import { Type } from 'class-transformer';
import { LendingStatusEnum } from '@app/lending/dao/lending-status.enum';

export class GetLendingListResult {
  @ApiProperty({
    type: 'string',
  })
  public readonly owner: AuthUserInterface;

  @ApiProperty()
  public readonly id: string;

  @ApiProperty({
    type: AccountResponse,
  })
  public readonly account: AccountResponse;

  @ApiProperty({
    type: 'string',
  })
  @Type(() => String)
  public readonly amount: bigint;

  @ApiProperty({
    type: 'string',
  })
  @Type(() => String)
  public readonly interestsAmount: bigint;

  @ApiProperty({
    type: Date,
    nullable: true,
  })
  public readonly lastInterestsAt: Date | null;

  @ApiProperty({
    enum: {
      IN_PROGRESS: LendingStatusEnum.IN_PROGRESS,
      CLOSED: LendingStatusEnum.CLOSED,
    },
    type: 'enum',
  })
  public readonly status: LendingStatusEnum;

  @ApiProperty()
  public readonly endDate: Date;

  @ApiProperty()
  public readonly createdAt: Date;

  constructor(lending: Lending, auth: AuthUserInterface) {
    this.id = lending.id;
    this.account = new AccountResponse(lending.account);
    this.amount = lending.amount;
    this.interestsAmount = lending.interestsAmount;
    this.lastInterestsAt = lending.lastInterestsAt;
    this.status = lending.status;
    this.endDate = lending.endDate;
    this.createdAt = lending.createdAt;
    this.owner = auth;
  }
}
