import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetHistoryQuery } from '@app/history/cqrs/get-history.query';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { PaginatedResult } from '@app/common/pagination/paginated-result';

@QueryHandler(GetHistoryQuery)
export class GetHistoryHandler
  implements IQueryHandler<GetHistoryQuery, PaginatedResult<Offer>>
{
  constructor(private readonly offerPgRepository: OfferPgRepository) {}

  public async execute(
    query: GetHistoryQuery,
  ): Promise<PaginatedResult<Offer>> {
    const [offers, total] = await this.offerPgRepository.getAffectedUserId(
      query.userId,
      query.pagination,
    );

    return {
      items: offers,
      pagination: query.pagination.withTotal(total),
    };
  }
}
