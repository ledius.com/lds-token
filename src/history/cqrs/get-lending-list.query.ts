import { IQuery } from '@nestjs/cqrs';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';
import { LendingStatusEnum } from '@app/lending/dao/lending-status.enum';

export class GetLendingListQuery implements IQuery {
  @IsOptional()
  @Type(() => PaginationDto)
  public pagination: PaginationDto;

  @ApiProperty({
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsUUID('4')
  public userId?: string;

  @ApiProperty({
    type: 'number',
    required: false,
  })
  @IsOptional()
  @Type(() => Number)
  public status?: LendingStatusEnum;
}
