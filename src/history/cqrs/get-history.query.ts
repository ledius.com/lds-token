import { IQuery } from '@nestjs/cqrs';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { IsDefined, IsOptional, IsUUID, ValidateNested } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetHistoryQuery implements IQuery {
  @IsDefined()
  @IsUUID('4')
  @ApiProperty()
  public readonly userId: string;

  @IsOptional()
  @ValidateNested()
  public readonly pagination: PaginationDto = new PaginationDto();

  constructor(userId: string, pagination: PaginationDto) {
    this.userId = userId;
    this.pagination = pagination;
  }

  public withPagination(pagination: PaginationDto): GetHistoryQuery {
    return new GetHistoryQuery(this.userId, pagination);
  }
}
