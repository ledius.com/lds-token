import { IQuery } from '@nestjs/cqrs';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { IsOptional, ValidateNested } from 'class-validator';

export class GetOffersQuery implements IQuery {
  @IsOptional()
  @ValidateNested()
  public readonly pagination: PaginationDto = new PaginationDto();

  constructor(pagination: PaginationDto) {
    this.pagination = pagination;
  }

  public withPagination(pagination: PaginationDto): GetOffersQuery {
    return new GetOffersQuery(pagination);
  }
}
