import { Module } from '@nestjs/common';
import { OfferModule } from '@app/offer/offer.module';
import { CqrsModule } from '@nestjs/cqrs';
import { HistoryController } from '@app/history/controller/history.controller';
import { GetHistoryHandler } from '@app/history/cqrs/get-history.handler';
import { HttpAuthModule } from '@app/http-auth/http-auth.module';
import { GetOffersHandler } from '@app/history/cqrs/get-offers.handler';
import { GetLendingListHandler } from '@app/history/cqrs/get-lending-list.handler';
import { LendingModule } from '@app/lending/lending.module';

@Module({
  controllers: [HistoryController],
  imports: [OfferModule, CqrsModule, HttpAuthModule, LendingModule],
  providers: [GetHistoryHandler, GetOffersHandler, GetLendingListHandler],
})
export class HistoryModule {}
