import { Module } from '@nestjs/common';
import { TransferController } from '@app/transfer/controller/transfer.controller';
import { SmartChainModule } from '@app/smart-chain/smart-chain.module';
import { AccountModule } from '@app/account/account.module';
import { CqrsModule } from '@nestjs/cqrs';
import { TransferHandler } from '@app/transfer/command/transfer.handler';

@Module({
  imports: [SmartChainModule, AccountModule, CqrsModule],
  controllers: [TransferController],
  providers: [TransferHandler],
  exports: [TransferHandler],
})
export class TransferModule {}
