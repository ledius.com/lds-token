import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { TransferCommand } from '@app/transfer/command/transfer.command';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { LediusContract } from '@app/smart-chain/contracts/ledius.contract';
import { TransactionReceiptInterface } from '@app/smart-chain/interfaces/transaction-receipt.interface';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface TransferResult extends TransactionReceiptInterface {}

@CommandHandler(TransferCommand)
export class TransferHandler
  implements ICommandHandler<TransferCommand, TransferResult>
{
  constructor(
    private readonly accountPgRepository: AccountPgRepository,
    private readonly lediusContract: LediusContract,
  ) {}

  public async execute(command: TransferCommand): Promise<TransferResult> {
    const sender = await this.accountPgRepository.getMainByUserId(
      command.sender,
    );
    const balance = await this.lediusContract.balanceOf(sender);
    if (balance < command.amount) {
      throw new BaseException({
        statusCode: ErrorCodeEnum.INSUFFICIENT_BALANCE,
        message: 'Insufficient balance',
      });
    }
    return await this.lediusContract.transfer(
      sender,
      { address: command.recipient },
      command.amount,
    );
  }
}
