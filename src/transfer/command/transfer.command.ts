import { ICommand } from '@nestjs/cqrs';
import { Type } from 'class-transformer';
import { IsDefined, IsEthereumAddress, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class TransferCommand implements ICommand {
  @IsDefined()
  @IsUUID('4')
  @ApiProperty({
    description: 'account user id',
  })
  readonly sender: string;

  @IsDefined()
  @IsEthereumAddress()
  @ApiProperty()
  readonly recipient: string;

  @IsDefined()
  @Type(() => BigInt)
  @ApiProperty({
    type: 'string',
  })
  readonly amount: bigint;
}
