import { TransferResult } from '@app/transfer/command/transfer.handler';
import { ApiProperty } from '@nestjs/swagger';

export class TransferResponse implements TransferResult {
  @ApiProperty()
  public readonly blockHash: string;

  @ApiProperty()
  public readonly from: string;

  @ApiProperty()
  public readonly to: string;

  @ApiProperty()
  public readonly transactionHash: string;

  constructor(transferResult: TransferResult) {
    this.blockHash = transferResult.blockHash;
    this.to = transferResult.to;
    this.from = transferResult.from;
    this.transactionHash = transferResult.transactionHash;
  }
}
