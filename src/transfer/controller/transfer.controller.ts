import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CommandBus } from '@nestjs/cqrs';
import { TransferCommand } from '@app/transfer/command/transfer.command';
import { TransferResult } from '@app/transfer/command/transfer.handler';
import { AuthPayload } from '@app/jwt/decorator/auth-payload';
import { TokenPayload } from '@app/jwt/interfaces/token-payload.interface';
import { AuthGuard } from '@app/jwt/guard/auth-guard';

@Controller()
@ApiTags('Transfer')
export class TransferController {
  constructor(private readonly commandBus: CommandBus) {}

  @HttpCode(200)
  @Post()
  @ApiOkResponse()
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  public async transfer(
    @Body() command: TransferCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<TransferResult> {
    if (command.sender !== tokenPayload.userId) {
      throw new BadRequestException('You can transfer only myself tokens');
    }
    return this.commandBus.execute<TransferCommand, TransferResult>(command);
  }
}
