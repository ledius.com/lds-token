import { Offer } from '@app/offer/dao/entity/offer';
import { FindOperator, LessThan, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { OfferTypeEnum } from '@app/offer/dao/offer-type.enum';
import { OfferStatusEnum } from '@app/offer/dao/offer-status.enum';
import { conditionsMapping } from '@app/common/conditions-mapping';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { typeormPagination } from '@app/common/typeorm/typeorm-pagination';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';
import { Account } from '@app/account/dao/entity/account';

export interface FindByOptions {
  readonly type?: OfferTypeEnum;
  readonly status?: OfferStatusEnum | FindOperator<OfferStatusEnum>;
  readonly owner?: {
    userId: string;
  };
  readonly amount?: bigint;
  readonly extra?: {
    readonly reservedAtLt?: Date;
    readonly pagination?: PaginationDto;
  };
}

@Injectable()
export class OfferPgRepository {
  constructor(
    @InjectRepository(Offer) private readonly repository: Repository<Offer>,
  ) {}

  public async getAffectedUserId(
    userId: string,
    pagination?: PaginationDto,
  ): Promise<[Offer[], number]> {
    const paginationOptions = typeormPagination(pagination);

    const qb = this.repository
      .createQueryBuilder('o')
      .leftJoinAndMapOne('owner', Account, 'owner', 'o.owner_id = owner.id')
      .leftJoinAndMapOne(
        'reservedBy',
        Account,
        'reservedBy',
        'o.reserved_by_id = reservedBy.id',
      )
      .where('owner.userId = :userId', { userId })
      .orWhere('reservedBy.userId = :userId', { userId })
      .take(paginationOptions.take)
      .skip(paginationOptions.skip)
      .addOrderBy('o.createdAt', 'DESC');

    return await qb.getManyAndCount();
  }

  public async findBy(options: FindByOptions): Promise<[Offer[], number]> {
    return this.repository.findAndCount({
      where: conditionsMapping<Offer>(options, {
        reservedAt: options?.extra?.reservedAtLt
          ? LessThan(options.extra.reservedAtLt)
          : undefined,
      }),
      ...typeormPagination(options.extra?.pagination),
      relations: ['owner'],
    });
  }

  public async save(offer: Offer): Promise<void> {
    await this.repository.save(offer);
  }

  public async getById(id: string): Promise<Offer> {
    const found = await this.repository.findOne({
      where: {
        id,
      },
    });
    if (!found) {
      throw new BaseException({
        message: 'Offer not found',
        statusCode: ErrorCodeEnum.NOT_FOUND,
      });
    }

    return found;
  }

  public async find(): Promise<Offer[]> {
    return this.repository.find();
  }
}
