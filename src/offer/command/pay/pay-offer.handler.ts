import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { PayOfferCommand } from '@app/offer/command/pay/pay-offer.command';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { PaymentService } from '@app/payment/service/payment.service';
import { CreatePaymentResult } from '@app/payment/service/create-payment.result';
import { ReserveOfferHandler } from '@app/offer/command/reserve/reserve-offer.handler';
import { OrderTypeEnum } from '@app/payment/order-type.enum';
import { NotificationUrlService } from '@app/payment/service/notification-url.service';

@CommandHandler(PayOfferCommand)
export class PayOfferHandler
  implements ICommandHandler<PayOfferCommand, CreatePaymentResult>
{
  constructor(
    private readonly offerPgRepository: OfferPgRepository,
    private readonly paymentService: PaymentService,
    private readonly reserveOfferHandler: ReserveOfferHandler,
    private readonly notificationUrlService: NotificationUrlService,
  ) {}

  public async execute(command: PayOfferCommand): Promise<CreatePaymentResult> {
    const reservedOffer = await this.reserveOfferHandler.execute(command);
    console.log(typeof reservedOffer.amount, reservedOffer.amount);
    return this.paymentService.createPayment(
      {
        Amount: reservedOffer.rate.price,
        OrderId: reservedOffer.id,
        DATA: {
          orderType: OrderTypeEnum.OFFER,
        },
        CustomerKey: reservedOffer.reservedBy.userId,
        NotificationURL:
          this.notificationUrlService.generate('/offers/confirm'),
        Receipt: {
          Items: [
            {
              Name: 'Пользовательские жетоны',
              Price: reservedOffer.rate.price,
              Tax: 'none',
              Amount: reservedOffer.rate.price,
              Quantity: 1,
            },
          ],
          Email: 'devsinglesly@gmail.com',
          Taxation: 'usn_income_outcome',
        },
      },
      command.accessToken,
    );
  }
}
