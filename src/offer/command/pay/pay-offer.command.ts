import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsUUID } from 'class-validator';

export class PayOfferCommand implements ICommand {
  @ApiProperty()
  @IsDefined()
  @IsUUID('4')
  public readonly id: string;

  @ApiProperty({
    description: 'user id',
  })
  @IsDefined()
  @IsUUID('4')
  public readonly reservedBy: string;

  public accessToken?: string;
}
