import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CloseOfferCommand } from '@app/offer/command/close/close-offer.command';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
@CommandHandler(CloseOfferCommand)
export class CloseOfferHandler
  implements ICommandHandler<CloseOfferCommand, Offer>
{
  constructor(private readonly offerPgRepository: OfferPgRepository) {}

  public async execute(command: CloseOfferCommand): Promise<Offer> {
    const offer = await this.offerPgRepository.getById(command.id);
    const closedOffer = offer.close();
    await this.offerPgRepository.save(closedOffer);

    return closedOffer;
  }
}
