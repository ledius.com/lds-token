import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsOptional, IsString, IsUUID } from 'class-validator';

export class CloseOfferCommand implements ICommand {
  @ApiProperty({
    description: 'offer id',
    type: 'string',
  })
  @IsDefined()
  @IsUUID('4')
  public readonly id: string;

  @ApiProperty({
    description: 'reject reason',
    type: 'string',
  })
  @IsOptional()
  @IsString()
  public readonly note: string = '';
}
