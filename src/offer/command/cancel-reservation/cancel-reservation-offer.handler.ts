import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CancelReservationOfferCommand } from '@app/offer/command/cancel-reservation/cancel-reservation-offer.command';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { OfferStatusEnum } from '@app/offer/dao/offer-status.enum';

export interface CancelReservationResult {
  cancelled: Offer[];
  reserved: Offer[];
}

@CommandHandler(CancelReservationOfferCommand)
export class CancelReservationOfferHandler
  implements
    ICommandHandler<CancelReservationOfferCommand, CancelReservationResult>
{
  constructor(private readonly offerPgRepository: OfferPgRepository) {}

  public async execute(
    command: CancelReservationOfferCommand,
  ): Promise<CancelReservationResult> {
    const [reservedOffers] = await this.offerPgRepository.findBy({
      status: OfferStatusEnum.RESERVED,
      extra: {
        reservedAtLt: command.expiredDate,
      },
    });
    const cancelledReservationOffers = reservedOffers
      .filter(Offer.hasNotBankDetails)
      .map(Offer.cancelReservation);

    for (const offer of cancelledReservationOffers) {
      await this.offerPgRepository.save(offer);
    }

    return {
      cancelled: cancelledReservationOffers,
      reserved: reservedOffers,
    };
  }
}
