import { ICommand } from '@nestjs/cqrs';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsDefined } from 'class-validator';

export class CancelReservationOfferCommand implements ICommand {
  @ApiProperty({
    type: 'string',
    example: new Date().toISOString(),
  })
  @IsDefined()
  @IsDateString()
  @Type(() => Date)
  public readonly expiredDate: Date;

  constructor(expiredDate: Date) {
    this.expiredDate = expiredDate;
  }
}
