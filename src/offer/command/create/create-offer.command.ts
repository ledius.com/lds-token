import { ICommand } from '@nestjs/cqrs';
import { Transform, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsDefined, IsInt } from 'class-validator';
import {
  ApiOfferTypeEnum,
  OfferTypeEnum,
} from '@app/offer/dao/offer-type.enum';

export class CreateOfferCommand implements ICommand {
  @ApiProperty({
    type: 'string',
  })
  @IsDefined()
  public readonly ownerId: string;

  @ApiProperty({
    type: 'string',
    example: '1000000000000000000',
  })
  @Type(() => BigInt)
  @Transform(({ value }) => BigInt(value), { toClassOnly: true })
  @IsDefined()
  public readonly amount: bigint;

  @ApiProperty()
  @Type(() => Number)
  @IsDefined()
  @IsInt()
  public readonly price: number;

  @ApiOfferTypeEnum()
  public readonly type: OfferTypeEnum;

  @ApiProperty({
    type: 'string',
    example: new Date().toISOString(),
  })
  @Type(() => Date)
  @IsDefined()
  @IsDate()
  public readonly publishAt: Date;
}
