import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateOfferCommand } from '@app/offer/command/create/create-offer.command';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { CurrencyEnum } from '@app/common/currency.enum';
import { HttpException, Injectable } from '@nestjs/common';
import { LediusContract } from '@app/smart-chain/contracts/ledius.contract';

@Injectable()
@CommandHandler(CreateOfferCommand)
export class CreateOfferHandler
  implements ICommandHandler<CreateOfferCommand, Offer>
{
  constructor(
    private readonly offerPgRepository: OfferPgRepository,
    private readonly accountPgRepository: AccountPgRepository,
    private readonly lediusContract: LediusContract,
  ) {}

  public async execute(command: CreateOfferCommand): Promise<Offer> {
    const owner = await this.accountPgRepository.getByUserId(command.ownerId);
    const availableBalance = await this.lediusContract.balanceOf(owner);
    if (availableBalance < command.amount) {
      throw new HttpException('Insufficient balance', 403);
    }
    const offer = Offer.new(
      owner,
      command.amount,
      {
        currency: CurrencyEnum.RUB,
        price: command.price,
      },
      command.type,
      command.publishAt,
    );

    await this.offerPgRepository.save(offer);

    return offer;
  }
}
