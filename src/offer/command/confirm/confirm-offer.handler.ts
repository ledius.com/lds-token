import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { ConfirmOfferCommand } from '@app/offer/command/confirm/confirm-offer.command';
import { Offer } from '@app/offer/dao/entity/offer';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { ContractFactoryService } from '@app/smart-chain/services/contract-factory.service';
import { BEP20TokenInterface } from '@app/smart-chain/BEP20-token.interface';
import { TokenSymbolEnum } from '@app/smart-chain/token-symbol.enum';
import { DispenseBnbService } from '@app/dispenser/service/dispense-bnb.service';

@CommandHandler(ConfirmOfferCommand)
export class ConfirmOfferHandler
  implements ICommandHandler<ConfirmOfferCommand, Offer>
{
  constructor(
    private readonly accountPgRepository: AccountPgRepository,
    private readonly offerPgRepository: OfferPgRepository,
    private readonly contractFactoryService: ContractFactoryService,
    private readonly dispenseBnbService: DispenseBnbService,
  ) {}

  private get lediusContract(): BEP20TokenInterface {
    return this.contractFactoryService.getContract(TokenSymbolEnum.LDS);
  }

  public async execute(command: ConfirmOfferCommand): Promise<Offer> {
    const [offer, account] = await Promise.all([
      this.offerPgRepository.getById(command.id),
      this.accountPgRepository.getByUserId(command.userId),
    ]);
    const availableBalance = await this.lediusContract.balanceOf(offer.owner);
    if (availableBalance < offer.amount) {
      throw new Error(
        `Insufficient balance: available ${availableBalance}, required ${offer.amount}`,
      );
    }
    const fulfilledOffer = offer.fulfilled();
    await this.offerPgRepository.save(fulfilledOffer);
    await this.lediusContract.transfer(offer.owner, account, offer.amount);
    await this.dispenseBnbService.dispenseFee(account);

    return fulfilledOffer;
  }
}
