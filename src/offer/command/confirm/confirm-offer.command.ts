import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';

export class ConfirmOfferCommand implements ICommand {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  public readonly userId: string;

  constructor(id: string, userId: string) {
    this.id = id;
    this.userId = userId;
  }
}
