import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetByIdOfferQuery } from '@app/offer/command/get-by-id/get-by-id-offer.query';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';

@QueryHandler(GetByIdOfferQuery)
export class GetByIdOfferHandler
  implements IQueryHandler<GetByIdOfferQuery, Offer>
{
  constructor(private readonly offerPgRepository: OfferPgRepository) {}

  public async execute(query: GetByIdOfferQuery): Promise<Offer> {
    return this.offerPgRepository.getById(query.offerId);
  }
}
