import { IsDefined, IsString } from 'class-validator';
import { IQuery } from '@nestjs/cqrs';

export class GetByIdOfferQuery implements IQuery {
  @IsDefined()
  @IsString()
  public readonly offerId: string;

  public constructor(offerId: string) {
    this.offerId = offerId;
  }
}
