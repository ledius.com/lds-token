import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { ReserveOfferCommand } from '@app/offer/command/reserve/reserve-offer.command';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { BankDetailsPgRepository } from '@app/bank-details/repository/bank-details-pg.repository';

@CommandHandler(ReserveOfferCommand)
export class ReserveOfferHandler
  implements ICommandHandler<ReserveOfferCommand, Offer>
{
  constructor(
    private readonly offerPgRepository: OfferPgRepository,
    private readonly accountPgRepository: AccountPgRepository,
    private readonly bankDetailsPgRepository: BankDetailsPgRepository,
  ) {}

  public async execute(command: ReserveOfferCommand): Promise<Offer> {
    const [offer, reservedBy] = await Promise.all([
      this.offerPgRepository.getById(command.id),
      this.accountPgRepository.getByUserId(command.reservedBy),
    ]);
    const reservedOffer = offer.reserve(reservedBy, new Date());
    if (command.bankDetails) {
      const bankDetails = await this.bankDetailsPgRepository.getById(
        command.bankDetails,
      );
      reservedOffer.addBankDetails(bankDetails);
    }
    await this.offerPgRepository.save(reservedOffer);

    return reservedOffer;
  }
}
