import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsOptional, IsUUID } from 'class-validator';

export class ReserveOfferCommand implements ICommand {
  @ApiProperty()
  @IsDefined()
  @IsUUID('4')
  public readonly id: string;

  @ApiProperty({
    description: 'user id',
  })
  @IsDefined()
  @IsUUID('4')
  public readonly reservedBy: string;

  @ApiProperty({
    type: 'string',
    required: false,
  })
  @IsOptional()
  public readonly bankDetails?: string;
}
