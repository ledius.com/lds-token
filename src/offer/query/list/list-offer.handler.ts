import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ListOfferQuery } from '@app/offer/query/list/list-offer.query';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { PaginatedResult } from '@app/common/pagination/paginated-result';

@QueryHandler(ListOfferQuery)
export class ListOfferHandler
  implements IQueryHandler<ListOfferQuery, PaginatedResult<Offer>>
{
  constructor(private readonly offerPgRepository: OfferPgRepository) {}

  public async execute(query: ListOfferQuery): Promise<PaginatedResult<Offer>> {
    const [items, total] = await this.offerPgRepository.findBy({
      type: query.type,
      status: query.status,
      extra: {
        pagination: query.pagination,
      },
    });

    return { items, pagination: query.pagination.withTotal(total) };
  }
}
