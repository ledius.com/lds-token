import { IQuery } from '@nestjs/cqrs';
import { OfferTypeEnum } from '@app/offer/dao/offer-type.enum';
import { OfferStatusEnum } from '@app/offer/dao/offer-status.enum';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { Type } from 'class-transformer';

export class ListOfferQuery implements IQuery {
  @ApiPropertyOptional({
    type: 'number',
    required: false,
  })
  @IsOptional()
  @Type(() => Number)
  public readonly type?: OfferTypeEnum;

  @ApiPropertyOptional({
    type: 'number',
    required: false,
  })
  @IsOptional()
  @Type(() => Number)
  public readonly status?: OfferStatusEnum;

  public readonly pagination?: PaginationDto;

  constructor(data: Partial<ListOfferQuery> = {}) {
    this.type = data.type;
    this.status = data.status;
    this.pagination = data.pagination;
  }

  public withPagination(pagination: PaginationDto): ListOfferQuery {
    return new ListOfferQuery({
      type: this.type,
      status: this.status,
      pagination,
    });
  }
}
