import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Cron, CronExpression } from '@nestjs/schedule';
import { CancelReservationOfferCommand } from '@app/offer/command/cancel-reservation/cancel-reservation-offer.command';
import { Offer } from '@app/offer/dao/entity/offer';
import { subMinutes } from 'date-fns';
import { CancelReservationResult } from '@app/offer/command/cancel-reservation/cancel-reservation-offer.handler';

@Injectable()
export class CancelReserveOfferTaskService {
  private readonly logger: Logger = new Logger(
    CancelReserveOfferTaskService.name,
  );

  constructor(private readonly commandBus: CommandBus) {}

  @Cron(CronExpression.EVERY_10_MINUTES)
  public async cancelReservation(): Promise<CancelReservationResult> {
    const cancelReservationOfferCommand = new CancelReservationOfferCommand(
      subMinutes(new Date(), 10),
    );
    this.logger.log(
      `Cancel reservation checkpoint [${cancelReservationOfferCommand.expiredDate.toISOString()}]`,
    );
    const { cancelled, reserved } = await this.commandBus.execute<
      CancelReservationOfferCommand,
      CancelReservationResult
    >(cancelReservationOfferCommand);
    for (const offer of reserved) {
      this.logger.log(
        `offer: [${
          offer.id
        }] reserved at [${offer.reservedAt.toISOString()}] was cancelled`,
      );
    }
    return { cancelled, reserved };
  }
}
