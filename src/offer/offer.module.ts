import { Module } from '@nestjs/common';
import { AccountModule } from '@app/account/account.module';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Offer } from '@app/offer/dao/entity/offer';
import { CreateOfferHandler } from '@app/offer/command/create/create-offer.handler';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { OfferController } from '@app/offer/controller/offer.controller';
import { SmartChainModule } from '@app/smart-chain/smart-chain.module';
import { ReserveOfferHandler } from '@app/offer/command/reserve/reserve-offer.handler';
import { ListOfferHandler } from '@app/offer/query/list/list-offer.handler';
import { PaymentModule } from '@app/payment/payment.module';
import { PayOfferHandler } from '@app/offer/command/pay/pay-offer.handler';
import { ConfirmOfferHandler } from '@app/offer/command/confirm/confirm-offer.handler';
import { CancelReservationOfferHandler } from '@app/offer/command/cancel-reservation/cancel-reservation-offer.handler';
import { CancelReserveOfferTaskService } from '@app/offer/service/cancel-reserve-offer-task.service';
import { BankDetailsModule } from '@app/bank-details/bank-details.module';
import { CloseOfferHandler } from '@app/offer/command/close/close-offer.handler';
import { GetByIdOfferHandler } from '@app/offer/command/get-by-id/get-by-id-offer.handler';
import { DispenserModule } from '@app/dispenser/dispenser.module';

@Module({
  imports: [
    AccountModule,
    CqrsModule,
    TypeOrmModule.forFeature([Offer]),
    SmartChainModule,
    PaymentModule,
    BankDetailsModule,
    DispenserModule,
  ],
  controllers: [OfferController],
  providers: [
    CreateOfferHandler,
    OfferPgRepository,
    ReserveOfferHandler,
    ListOfferHandler,
    PayOfferHandler,
    ConfirmOfferHandler,
    CancelReservationOfferHandler,
    CancelReserveOfferTaskService,
    CloseOfferHandler,
    GetByIdOfferHandler,
  ],
  exports: [OfferPgRepository, CreateOfferHandler],
})
export class OfferModule {}
