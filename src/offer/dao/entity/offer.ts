import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CurrencyEnum } from '@app/common/currency.enum';
import { OfferStatusEnum } from '@app/offer/dao/offer-status.enum';
import { Account } from '@app/account/dao/entity/account';
import { v4 } from 'uuid';
import { OfferTypeEnum } from '@app/offer/dao/offer-type.enum';
import { BankDetails } from '@app/bank-details/dao/entity/bank-details';

const TABLE_NAME = 'offers';

export interface Rate {
  currency: CurrencyEnum;
  price: number;
}

@Entity(TABLE_NAME)
export class Offer {
  public static readonly tableName: string = TABLE_NAME;

  @PrimaryColumn('uuid')
  public readonly id: string;

  @Column('decimal', {
    precision: 36,
    transformer: {
      from(value: string): BigInt {
        return BigInt(value);
      },
      to(value: BigInt | string): string {
        return String(value);
      },
    },
  })
  public readonly amount: bigint;

  @Column('json')
  public readonly rate: Rate;

  @Column('smallint')
  public readonly status: OfferStatusEnum;

  @Column('smallint', {
    default: OfferTypeEnum.BUY,
  })
  public readonly type: OfferTypeEnum;

  @ManyToOne(() => Account, {
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    eager: true,
  })
  public readonly owner: Account;

  @ManyToOne(() => Account, {
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    eager: true,
  })
  public readonly reservedBy?: Account | null;

  @Column('timestamp with time zone', {
    nullable: true,
  })
  public readonly reservedAt?: Date | null;

  @ManyToOne(() => BankDetails, {
    onDelete: 'RESTRICT',
    eager: true,
    onUpdate: 'RESTRICT',
    nullable: true,
  })
  public bankDetails?: BankDetails;

  @Column('timestamp with time zone')
  public readonly publishedAt: Date;

  @CreateDateColumn()
  public readonly createdAt: Date;

  @UpdateDateColumn()
  public readonly updatedAt: Date;

  private constructor(
    id: string,
    owner: Account,
    amount: bigint,
    rate: Rate,
    status: OfferStatusEnum,
    type: OfferTypeEnum,
    publishedAt: Date,
    reservedBy: Account = null,
    reservedAt: Date = null,
    bankDetails: BankDetails = null,
  ) {
    this.id = id;
    this.owner = owner;
    this.amount = amount;
    this.rate = rate;
    this.status = status;
    this.type = type;
    this.publishedAt = publishedAt;
    this.reservedBy = reservedBy;
    this.reservedAt = reservedAt;
    this.bankDetails = bankDetails;
  }

  public static new(
    owner: Account,
    amount: bigint,
    rate: Rate,
    type: OfferTypeEnum,
    publishedAt: Date,
  ): Offer {
    return new Offer(
      v4(),
      owner,
      amount,
      rate,
      OfferStatusEnum.OPENED,
      type,
      publishedAt,
    );
  }

  public reserve(reservedBy: Account, reservedAt: Date): Offer {
    if (!this.isOpened()) {
      throw new Error('Only opened offer can be reserve');
    }
    return new Offer(
      this.id,
      this.owner,
      this.amount,
      this.rate,
      OfferStatusEnum.RESERVED,
      this.type,
      this.publishedAt,
      reservedBy,
      reservedAt,
    );
  }

  public static cancelReservation(offer: Offer): Offer {
    if (!offer.isReserved()) {
      throw new Error('Offer must be reserved before cancel');
    }
    return new Offer(
      offer.id,
      offer.owner,
      offer.amount,
      offer.rate,
      OfferStatusEnum.OPENED,
      offer.type,
      offer.publishedAt,
    );
  }

  public cancelReservation(): Offer {
    if (!this.isReserved()) {
      throw new Error('Offer must be reserved before cancel');
    }
    return new Offer(
      this.id,
      this.owner,
      this.amount,
      this.rate,
      OfferStatusEnum.OPENED,
      this.type,
      this.publishedAt,
    );
  }

  public fulfilled(): Offer {
    if (!this.isReserved()) {
      throw new Error('Only reserved offer can be fulfilled');
    }
    return new Offer(
      this.id,
      this.owner,
      this.amount,
      this.rate,
      OfferStatusEnum.FULFILLED,
      this.type,
      this.publishedAt,
    );
  }

  public close(): Offer {
    return new Offer(
      this.id,
      this.owner,
      this.amount,
      this.rate,
      OfferStatusEnum.CLOSED,
      this.type,
      this.publishedAt,
      this.reservedBy,
      this.reservedAt,
    );
  }

  public addBankDetails(bankDetails: BankDetails): void {
    this.bankDetails = bankDetails;
  }

  public static hasNotBankDetails(offer: Offer): boolean {
    return !offer.hasBankDetails();
  }

  public hasBankDetails(): boolean {
    return Boolean(this.bankDetails);
  }

  public isReserved(): boolean {
    return this.status === OfferStatusEnum.RESERVED;
  }

  public isOpened(): boolean {
    return this.status === OfferStatusEnum.OPENED;
  }
}
