export enum OfferStatusEnum {
  OPENED,
  RESERVED,
  FULFILLED,
  CLOSED,
}
