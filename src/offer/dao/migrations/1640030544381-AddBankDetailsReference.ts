import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddBankDetailsReference1640030544381
  implements MigrationInterface
{
  name = 'AddBankDetailsReference1640030544381';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "offers" ADD "bank_details_id" uuid`);
    await queryRunner.query(
      `ALTER TABLE "offers" ADD CONSTRAINT "FK_e02af8c0727070627b259254745" FOREIGN KEY ("bank_details_id") REFERENCES "bank-details"("id") ON DELETE RESTRICT ON UPDATE RESTRICT`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "offers" DROP CONSTRAINT "FK_e02af8c0727070627b259254745"`,
    );
    await queryRunner.query(
      `ALTER TABLE "offers" DROP COLUMN "bank_details_id"`,
    );
  }
}
