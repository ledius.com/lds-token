import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateOffersTable1639054607070 implements MigrationInterface {
  public readonly name = 'CreateOffersTable1639054607070';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "offers" (
            "id" uuid NOT NULL,
            "amount" numeric(36) NOT NULL,
            "rate" json NOT NULL,
            "status" smallint NOT NULL,
            "reserved_at" TIMESTAMP WITH TIME ZONE,
            "published_at" TIMESTAMP WITH TIME ZONE NOT NULL,
            "created_at" TIMESTAMP NOT NULL DEFAULT now(),
            "updated_at" TIMESTAMP NOT NULL DEFAULT now(),
            "owner_id" uuid, 
            "reserved_by_id" uuid,
             CONSTRAINT "PK_4c88e956195bba85977da21b8f4" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "offers" ADD CONSTRAINT "FK_e1e2232f2458ed1b0bb6bdb6ba1" FOREIGN KEY ("owner_id") REFERENCES "accounts"("id") ON DELETE RESTRICT ON UPDATE RESTRICT`,
    );
    await queryRunner.query(
      `ALTER TABLE "offers" ADD CONSTRAINT "FK_a4231cc2a775fa8d8514b107e13" FOREIGN KEY ("reserved_by_id") REFERENCES "accounts"("id") ON DELETE RESTRICT ON UPDATE RESTRICT`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "offers" DROP CONSTRAINT "FK_a4231cc2a775fa8d8514b107e13"`,
    );
    await queryRunner.query(
      `ALTER TABLE "offers" DROP CONSTRAINT "FK_e1e2232f2458ed1b0bb6bdb6ba1"`,
    );
    await queryRunner.query(`DROP TABLE "offers"`);
  }
}
