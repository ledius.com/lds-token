import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddTypeColumnToOffer1639931234705 implements MigrationInterface {
  public readonly name = 'AddTypeColumnToOffer1639931234705';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "offers" ADD "type" smallint NOT NULL DEFAULT 0`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "offers" DROP COLUMN "type"`);
  }
}
