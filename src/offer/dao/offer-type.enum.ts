import { createApiPropertyDecorator } from '@nestjs/swagger/dist/decorators/api-property.decorator';
import { Type } from 'class-transformer';
import { IsDefined, IsEnum } from 'class-validator';
import { composePropertyDecorators } from '@app/common/compose-property-decorators';

export enum OfferTypeEnum {
  BUY,
  SELL,
}

export function ApiOfferTypeEnum(): PropertyDecorator {
  return composePropertyDecorators(
    createApiPropertyDecorator({
      type: 'integer',
      enum: OfferTypeEnum,
      enumName: 'OfferTypeEnum',
    }),
    Type(() => Number),
    IsDefined(),
    IsEnum(OfferTypeEnum),
  );
}
