import { Offer, Rate as OfferRate } from '@app/offer/dao/entity/offer';
import { CurrencyEnum } from '@app/common/currency.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { OfferStatusEnum } from '@app/offer/dao/offer-status.enum';
import { AccountResponse } from '@app/account/controller/account.response';
import { OfferTypeEnum } from '@app/offer/dao/offer-type.enum';
import { BankDetailsResponse } from '@app/bank-details/controller/bank-details.response';

export class Rate {
  @ApiProperty()
  public readonly currency: CurrencyEnum;

  @ApiProperty()
  public readonly price: number;

  constructor(rate: OfferRate) {
    this.currency = rate.currency;
    this.price = rate.price;
  }
}

export class OfferResponse {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  public readonly amount: string;

  @ApiProperty({
    type: Rate,
  })
  @Type(() => Rate)
  public readonly rate: Rate;

  @ApiProperty({
    type: 'integer',
    enum: OfferStatusEnum,
    enumName: 'OfferStatusEnum',
  })
  public readonly status: OfferStatusEnum;

  @ApiProperty({
    type: 'integer',
    enum: OfferTypeEnum,
    enumName: 'OfferTypeEnum',
  })
  public readonly type: OfferTypeEnum;

  @ApiProperty({
    type: AccountResponse,
  })
  @Type(() => AccountResponse)
  public readonly owner: AccountResponse;

  @ApiProperty({
    type: AccountResponse,
    nullable: true,
  })
  @Type(() => AccountResponse)
  public readonly reservedBy: AccountResponse | null;

  @ApiProperty({
    type: Date,
    nullable: true,
  })
  public readonly reservedAt: Date | null;

  @ApiProperty({
    type: BankDetailsResponse,
    nullable: true,
  })
  @Type(() => BankDetailsResponse)
  public readonly bankDetails: BankDetailsResponse | null;

  @ApiProperty({
    type: Date,
  })
  public readonly publishedAt: Date;

  @ApiProperty({
    type: Date,
  })
  public readonly createdAt: Date;

  @ApiProperty({
    type: Date,
  })
  public readonly updatedAt: Date;

  constructor(offer: Offer) {
    this.id = offer.id;
    this.owner = offer.owner && new AccountResponse(offer.owner);
    this.amount = offer.amount.toString();
    this.rate = new Rate(offer.rate);
    this.reservedBy = offer.reservedBy && new AccountResponse(offer.reservedBy);
    this.reservedAt = offer.reservedAt || null;
    this.bankDetails =
      offer.bankDetails && new BankDetailsResponse(offer.bankDetails);
    this.publishedAt = offer.publishedAt;
    this.createdAt = offer.createdAt;
    this.updatedAt = offer.updatedAt;
    this.status = offer.status;
    this.type = offer.type;
  }
}
