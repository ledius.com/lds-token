import { ApiProperty } from '@nestjs/swagger';
import { OfferResponse } from '@app/offer/controller/offer.response';
import { PaginatedResult } from '@app/common/pagination/paginated-result';
import { Offer } from '@app/offer/dao/entity/offer';
import { PaginationResponse } from '@app/common/pagination/pagination.response';

export class OfferListResponse {
  @ApiProperty({
    type: PaginationResponse,
  })
  public readonly pagination: PaginationResponse;

  @ApiProperty({
    type: OfferResponse,
    isArray: true,
  })
  public readonly items: OfferResponse[];

  public constructor({ pagination, items }: PaginatedResult<Offer>) {
    this.pagination = pagination;
    this.items = items.map((offer) => new OfferResponse(offer));
  }
}
