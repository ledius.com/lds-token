import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateOfferCommand } from '@app/offer/command/create/create-offer.command';
import { Offer } from '../dao/entity/offer';
import { OfferResponse } from '@app/offer/controller/offer.response';
import { ReserveOfferCommand } from '@app/offer/command/reserve/reserve-offer.command';
import { ListOfferQuery } from '@app/offer/query/list/list-offer.query';
import { CreatePaymentResult } from '@app/payment/service/create-payment.result';
import { PayOfferCommand } from '@app/offer/command/pay/pay-offer.command';
import { PaymentNotification } from '@app/payment/payment.notification';
import { PaymentStatusEnum } from '@app/payment/payment-status.enum';
import { OrderTypeEnum } from '@app/payment/order-type.enum';
import { ConfirmOfferCommand } from '@app/offer/command/confirm/confirm-offer.command';
import { AuthPayload } from '@app/jwt/decorator/auth-payload';
import { TokenPayload } from '@app/jwt/interfaces/token-payload.interface';
import { AuthGuard } from '@app/jwt/guard/auth-guard';
import { CloseOfferCommand } from '@app/offer/command/close/close-offer.command';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { QueryPagination } from '@app/common/pagination/query-pagination';
import { ApiPaginated } from '@app/common/pagination/api-paginated';
import { OfferListResponse } from '@app/offer/controller/offer-list.response';
import { GetByIdOfferQuery } from '@app/offer/command/get-by-id/get-by-id-offer.query';

@Controller()
@ApiTags('Offer')
export class OfferController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get()
  @ApiOkResponse({
    type: OfferListResponse,
  })
  @ApiPaginated()
  public async list(
    @Query() query: ListOfferQuery,
    @QueryPagination() pagination: PaginationDto,
  ): Promise<OfferListResponse> {
    return new OfferListResponse(
      await this.queryBus.execute(query.withPagination(pagination)),
    );
  }

  @Get(':offerId')
  @ApiOkResponse({
    type: OfferResponse,
  })
  public async getById(
    @Param('offerId') offerId: string,
  ): Promise<OfferResponse> {
    return new OfferResponse(
      await this.queryBus.execute(new GetByIdOfferQuery(offerId)),
    );
  }

  @Post()
  @ApiCreatedResponse({
    type: OfferResponse,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  public async create(
    @Body() command: CreateOfferCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<OfferResponse> {
    if (command.ownerId !== tokenPayload.userId) {
      throw new BadRequestException('You can place only myself offers');
    }
    return new OfferResponse(
      await this.commandBus.execute<CreateOfferCommand, Offer>(command),
    );
  }

  @Patch('reserve')
  @ApiOkResponse({
    type: OfferResponse,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  public async reserve(
    @Body() command: ReserveOfferCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<OfferResponse> {
    if (command.reservedBy !== tokenPayload.userId) {
      throw new BadRequestException('You can reserve only by myself');
    }
    return new OfferResponse(
      await this.commandBus.execute<ReserveOfferCommand, Offer>(command),
    );
  }

  @Patch('reserve/confirm')
  @ApiOkResponse({
    type: OfferResponse,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  public async reserveConfirm(
    @Body() command: ConfirmOfferCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<OfferResponse> {
    if (command.userId !== tokenPayload.userId) {
      throw new BadRequestException('You can confirm only by myself offers');
    }
    return new OfferResponse(await this.commandBus.execute(command));
  }

  @Patch('pay')
  @ApiOkResponse({
    type: CreatePaymentResult,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  public async pay(
    @Body() command: PayOfferCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<CreatePaymentResult> {
    if (command.reservedBy !== tokenPayload.userId) {
      throw new BadRequestException('You can reserve only by myself offers');
    }
    command.accessToken = tokenPayload.token;
    return this.commandBus.execute<PayOfferCommand, CreatePaymentResult>(
      command,
    );
  }

  @HttpCode(200)
  @Post('confirm')
  public async payConfirm(
    @Body() notification: PaymentNotification,
  ): Promise<'OK'> {
    console.log(JSON.stringify(notification));
    if (
      notification.Success &&
      notification.Status === PaymentStatusEnum.CONFIRMED &&
      notification.Data?.orderType === OrderTypeEnum.OFFER
    ) {
      await this.commandBus.execute(
        new ConfirmOfferCommand(notification.OrderId, notification.Data.userId),
      );
    }
    return 'OK';
  }

  @Patch('close')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOkResponse({
    type: OfferResponse,
  })
  public async close(
    @Body() command: CloseOfferCommand,
  ): Promise<OfferResponse> {
    return new OfferResponse(await this.commandBus.execute(command));
  }
}
