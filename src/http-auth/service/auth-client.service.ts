import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';
import { DISPENSER_ACCOUNT_USER_ID } from '@app/dispenser/service/dispense-account.service';

export interface AuthUserInterface {
  readonly name: string;
  readonly email: string;
  readonly id: string;
  readonly picture: string;
  readonly roles: string[];
}

@Injectable()
export class AuthClientService {
  private readonly excludeIds: string[] = [DISPENSER_ACCOUNT_USER_ID];

  constructor(private readonly http: HttpService) {}

  private safeIds(ids: string[]): string[] {
    return ids.filter((id) => !this.excludeIds.includes(id));
  }

  public async getUsersByIds(ids: string[]): Promise<AuthUserInterface[]> {
    const safeIds = this.safeIds(ids);
    const response = safeIds.length
      ? await lastValueFrom(
          this.http.get<AuthUserInterface[]>('/internal/auth/users', {
            params: {
              ids: this.safeIds(ids).join(','),
            },
          }),
        )
      : { data: [] as AuthUserInterface[] };

    if (ids.includes(DISPENSER_ACCOUNT_USER_ID)) {
      response.data.push({
        id: DISPENSER_ACCOUNT_USER_ID,
        roles: [],
        picture: '',
        email: 'dispenser@ledius.ru',
        name: 'Dispenser Account',
      });
    }

    return response.data;
  }

  public async getUserById(id: string): Promise<AuthUserInterface> {
    const users = await this.getUsersByIds([id]);
    const user = users[0];
    if (!user) {
      throw new BaseException({
        statusCode: ErrorCodeEnum.NOT_FOUND,
        message: 'User not found',
      });
    }

    return user;
  }
}
