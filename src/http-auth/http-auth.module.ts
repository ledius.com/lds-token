import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SD_COURSES_BASE_URL } from '@app/http-auth/constants';
import { HttpModule } from '@nestjs/axios';
import { AuthClientService } from '@app/http-auth/service/auth-client.service';

@Module({
  imports: [
    ConfigModule,
    HttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => {
        return {
          baseURL: config.get(SD_COURSES_BASE_URL),
          timeout: 5000,
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [
    AuthClientService,
    {
      provide: SD_COURSES_BASE_URL,
      useFactory: (config: ConfigService) => config.get(SD_COURSES_BASE_URL),
      inject: [ConfigService],
    },
  ],
  exports: [AuthClientService],
})
export class HttpAuthModule {}
