import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { swaggerPlugin } from '@app/swagger-plugin';
import { swaggerInternalPlugin } from '@app/internal/swagger-internal-plugin';

(async () => {
  const app = await NestFactory.create(AppModule);

  swaggerPlugin(app);
  swaggerInternalPlugin(app);
  await app.listen(3000);
})();
