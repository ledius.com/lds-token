import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateBankDetailsTable1639904921788 implements MigrationInterface {
  public readonly name = 'CreateBankDetailsTable1639904921788';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "bank-details" (
            "id" uuid NOT NULL,
            "user_id" uuid NOT NULL,
            "card" character varying NOT NULL, 
            "name" text NOT NULL, 
            "bank" character varying NOT NULL, 
            CONSTRAINT "PK_66d28e3dc81f26a38db6a534294" PRIMARY KEY ("id")
        )`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "bank-details"`);
  }
}
