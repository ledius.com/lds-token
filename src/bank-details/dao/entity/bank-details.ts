import { Column, Entity, PrimaryColumn } from 'typeorm';
import { v4 } from 'uuid';

export interface Name {
  first: string;
  last: string;
  patronymic: string;
}

@Entity('bank-details')
export class BankDetails {
  @PrimaryColumn({
    type: 'uuid',
  })
  public readonly id: string;

  @Column({
    type: 'uuid',
  })
  public readonly userId: string;

  @Column()
  public readonly card: string;

  @Column('simple-json')
  public readonly name: Name;

  @Column()
  public readonly bank: string;

  constructor(userId: string, card: string, bank: string, name: Name) {
    this.id = v4();
    this.userId = userId;
    this.card = card;
    this.bank = bank;
    this.name = name;
  }

  public getSecureCard(): string {
    return this.card.replace(/\s/g, '');
  }
}
