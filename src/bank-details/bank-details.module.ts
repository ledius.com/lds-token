import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { BankDetailsPgRepository } from '@app/bank-details/repository/bank-details-pg.repository';
import { BankDetailsEnsureExistsHandler } from '@app/bank-details/command/ensure-exists/bank-details-ensure-exists.handler';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BankDetails } from '@app/bank-details/dao/entity/bank-details';
import { BankDetailsController } from '@app/bank-details/controller/bank-details.controller';
import { BankDetailsListHandler } from '@app/bank-details/query/list/bank-details-list.handler';

@Module({
  imports: [CqrsModule, TypeOrmModule.forFeature([BankDetails])],
  controllers: [BankDetailsController],
  providers: [
    BankDetailsPgRepository,
    BankDetailsEnsureExistsHandler,
    BankDetailsListHandler,
  ],
  exports: [
    BankDetailsEnsureExistsHandler,
    BankDetailsPgRepository,
    BankDetailsListHandler,
  ],
})
export class BankDetailsModule {}
