import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { BankDetailsListQuery } from '@app/bank-details/query/list/bank-details-list.query';
import { BankDetails } from '@app/bank-details/dao/entity/bank-details';
import { BankDetailsPgRepository } from '@app/bank-details/repository/bank-details-pg.repository';

@QueryHandler(BankDetailsListQuery)
export class BankDetailsListHandler
  implements IQueryHandler<BankDetailsListQuery, BankDetails[]>
{
  constructor(
    private readonly bankDetailsPgRepository: BankDetailsPgRepository,
  ) {}

  public async execute(query: BankDetailsListQuery): Promise<BankDetails[]> {
    return this.bankDetailsPgRepository.findBy({
      userId: query.userId,
    });
  }
}
