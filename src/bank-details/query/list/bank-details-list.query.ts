import { IQuery } from '@nestjs/cqrs';
import { IsOptional, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class BankDetailsListQuery implements IQuery {
  @IsOptional()
  @ApiProperty({
    required: false,
  })
  @IsUUID('4')
  public readonly userId?: string;
}
