import { BankDetails, Name } from '@app/bank-details/dao/entity/bank-details';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class BankDetailsName implements Name {
  @ApiProperty()
  public readonly first: string;

  @ApiProperty()
  public readonly last: string;

  @ApiProperty()
  public readonly patronymic: string;

  constructor(name: Name) {
    this.first = name.first;
    this.patronymic = name.patronymic;
    this.last = name.last;
  }
}

export class BankDetailsResponse {
  @ApiProperty()
  private readonly id: string;

  @ApiProperty()
  private readonly userId: string;

  @ApiProperty()
  private readonly card: string;

  @ApiProperty()
  @Type(() => BankDetailsName)
  private readonly name: BankDetailsName;

  @ApiProperty()
  private readonly bank: string;

  constructor(bankDetails: BankDetails) {
    this.id = bankDetails.id;
    this.name = new BankDetailsName(bankDetails.name);
    this.card = bankDetails.getSecureCard();
    this.bank = bankDetails.bank;
    this.userId = bankDetails.userId;
  }
}
