import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { BankDetailsEnsureExistsCommand } from '@app/bank-details/command/ensure-exists/bank-details-ensure-exists.command';
import { BankDetailsResponse } from '@app/bank-details/controller/bank-details.response';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { BankDetailsListQuery } from '@app/bank-details/query/list/bank-details-list.query';
import { BankDetails } from '@app/bank-details/dao/entity/bank-details';
import { AuthGuard } from '@app/jwt/guard/auth-guard';
import { AuthPayload } from '@app/jwt/decorator/auth-payload';
import { TokenPayload } from '@app/jwt/interfaces/token-payload.interface';

@Controller()
@ApiTags('Bank Details')
export class BankDetailsController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('ensure')
  @HttpCode(200)
  @ApiOkResponse({
    type: BankDetailsResponse,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  public async ensure(
    @Body() command: BankDetailsEnsureExistsCommand,
    @AuthPayload() tokenPayload: TokenPayload,
  ): Promise<BankDetailsResponse> {
    if (command.userId !== tokenPayload.userId) {
      throw new BadRequestException('You cannot ensure this bank details');
    }
    return new BankDetailsResponse(await this.commandBus.execute(command));
  }

  @Get()
  @ApiOkResponse({
    type: BankDetailsResponse,
    isArray: true,
  })
  public async list(
    @Query() query: BankDetailsListQuery,
  ): Promise<BankDetailsResponse[]> {
    const bankDetails = await this.queryBus.execute<
      BankDetailsListQuery,
      BankDetails[]
    >(query);

    return bankDetails.map((bankDetail) => new BankDetailsResponse(bankDetail));
  }
}
