import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BankDetailsEnsureExistsCommand } from '@app/bank-details/command/ensure-exists/bank-details-ensure-exists.command';
import { BankDetails } from '@app/bank-details/dao/entity/bank-details';
import { BankDetailsPgRepository } from '@app/bank-details/repository/bank-details-pg.repository';

@CommandHandler(BankDetailsEnsureExistsCommand)
export class BankDetailsEnsureExistsHandler
  implements ICommandHandler<BankDetailsEnsureExistsCommand, BankDetails>
{
  constructor(
    private readonly bankDetailsPgRepository: BankDetailsPgRepository,
  ) {}

  public async execute(
    command: BankDetailsEnsureExistsCommand,
  ): Promise<BankDetails> {
    const found = await this.bankDetailsPgRepository.findOneBy({
      bank: command.bank,
      userId: command.userId,
      card: command.card,
    });
    if (found) {
      return found;
    }

    const bankDetails = new BankDetails(
      command.userId,
      command.card,
      command.bank,
      {
        first: command.firstName,
        last: command.lastName,
        patronymic: command.patronymic,
      },
    );

    await this.bankDetailsPgRepository.save(bankDetails);

    return bankDetails;
  }
}
