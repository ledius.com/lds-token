import { ICommand } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { v4 } from 'uuid';
import { IsDefined, IsString, IsUUID } from 'class-validator';

export class BankDetailsEnsureExistsCommand implements ICommand {
  @ApiProperty({
    example: v4(),
  })
  @IsDefined()
  @IsUUID('4')
  public readonly userId: string;

  @ApiProperty({
    example: '4276450013997979',
  })
  @IsDefined()
  @IsString()
  public readonly card: string;

  @ApiProperty({
    example: 'Sberbank',
  })
  @IsDefined()
  @IsString()
  public readonly bank: string;

  @ApiProperty({
    example: 'Артем',
  })
  @IsDefined()
  @IsString()
  public readonly firstName: string;

  @ApiProperty({
    example: 'Ильиных',
  })
  @IsDefined()
  @IsString()
  public readonly lastName: string;

  @ApiProperty({
    example: 'Александрович',
  })
  @IsDefined()
  @IsString()
  public readonly patronymic: string;
}
