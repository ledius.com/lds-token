import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BankDetails } from '@app/bank-details/dao/entity/bank-details';
import { Repository } from 'typeorm';
import { conditionsMapping } from '@app/common/conditions-mapping';

export interface FindByOptions {
  readonly userId?: string;
  readonly card?: string;
  readonly bank?: string;
}

@Injectable()
export class BankDetailsPgRepository {
  constructor(
    @InjectRepository(BankDetails)
    private readonly bankDetailsRepository: Repository<BankDetails>,
  ) {}

  public async findBy(options: FindByOptions): Promise<BankDetails[]> {
    return this.bankDetailsRepository.find({
      where: conditionsMapping(options),
    });
  }

  public async findOneBy(
    options: FindByOptions,
  ): Promise<BankDetails | undefined> {
    return this.bankDetailsRepository.findOne({
      where: conditionsMapping(options),
    });
  }

  public async getById(id: string): Promise<BankDetails> {
    const found = await this.bankDetailsRepository.findOne({
      where: { id },
    });
    if (!found) {
      throw new NotFoundException('Bank details not found');
    }
    return found;
  }

  public async save(bankDetails: BankDetails): Promise<void> {
    await this.bankDetailsRepository.save(bankDetails);
  }
}
