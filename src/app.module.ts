import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SmartChainModule } from '@app/smart-chain/smart-chain.module';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountModule } from '@app/account/account.module';
import { APP_FILTER, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { TransferModule } from '@app/transfer/transfer.module';
import { BalanceModule } from '@app/balance/balance.module';
import { OfferModule } from '@app/offer/offer.module';
import { BankDetailsModule } from '@app/bank-details/bank-details.module';
import { ScheduleModule } from '@nestjs/schedule';
import { JsonWebTokenModule } from '@app/jwt/json-web-token.module';
import { InternalModule } from '@app/internal/internal.module';
import { BaseExceptionFilter } from '@app/common/filters/base-exception.filter';
import { LendingModule } from '@app/lending/lending.module';
import { Router } from '@app/router';
import { DispenserModule } from '@app/dispenser/dispenser.module';
import { HistoryModule } from '@app/history/history.module';
import { UsersModule } from '@app/users/users.module';
import { OfferTemplateModule } from '@app/offer-template/offer-template.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => {
        return {
          type: 'postgres',
          username: config.get('POSTGRES_USER'),
          password: config.get('POSTGRES_PASS'),
          host: config.get('POSTGRES_HOST'),
          port: Number(config.get('POSTGRES_PORT')),
          database: config.get('POSTGRES_DB'),
          autoLoadEntities: true,
          keepConnectionAlive: true,
          namingStrategy: new SnakeNamingStrategy(),
          ssl:
            config.get('POSTGRES_USE_SSL') === '1'
              ? {
                  rejectUnauthorized: false,
                  ca: config.get('POSTGRES_SSL_CA'),
                }
              : false,
        };
      },
      inject: [ConfigService],
    }),
    JsonWebTokenModule.forRoot({}),
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    SmartChainModule,
    AccountModule,
    BalanceModule,
    TransferModule,
    OfferModule,
    BankDetailsModule,
    LendingModule,
    InternalModule,
    DispenserModule,
    HistoryModule,
    UsersModule,
    OfferTemplateModule,
    Router,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({ transform: true }),
    },
    {
      provide: APP_FILTER,
      useClass: BaseExceptionFilter,
    },
  ],
})
export class AppModule {}
