import { Module } from '@nestjs/common';
import { TransferModule } from '@app/transfer/transfer.module';
import { AccountModule } from '@app/account/account.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lending } from '@app/lending/dao/entity/lending';
import { CqrsModule } from '@nestjs/cqrs';
import { LendingCreateHandler } from '@app/lending/command/create/lending-create.handler';
import { LendingPgRepository } from '@app/lending/repository/lending-pg.repository';
import { LendingListHandler } from '@app/lending/query/list/lending-list.handler';
import { LendingController } from '@app/lending/controller/lending.controller';
import { LendingAccruePercentService } from '@app/lending/service/lending-accrue-percent.service';
import { LendingAccrueHandler } from '@app/lending/command/accrue/lending-accrue.handler';
import { ConfigModule } from '@nestjs/config';
import { SmartChainModule } from '@app/smart-chain/smart-chain.module';
import { LendingAccrueTaskService } from '@app/lending/service/lending-accrue-task.service';
import { DispenserModule } from '@app/dispenser/dispenser.module';
import { ReturnLendingHandler } from '@app/lending/command/return/return-lending.handler';

@Module({
  imports: [
    DispenserModule,
    TransferModule,
    AccountModule,
    TypeOrmModule.forFeature([Lending]),
    CqrsModule,
    ConfigModule,
    SmartChainModule,
  ],
  controllers: [LendingController],
  providers: [
    LendingCreateHandler,
    LendingPgRepository,
    LendingListHandler,
    LendingAccruePercentService,
    LendingAccrueHandler,
    LendingAccrueTaskService,
    ReturnLendingHandler,
  ],
  exports: [LendingCreateHandler, LendingPgRepository, LendingListHandler],
})
export class LendingModule {}
