import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Lending } from '@app/lending/dao/entity/lending';
import { InjectRepository } from '@nestjs/typeorm';
import { LendingStatusEnum } from '@app/lending/dao/lending-status.enum';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';
import { PaginationDto } from '@app/common/pagination/pagination.dto';
import { typeormPagination } from '@app/common/typeorm/typeorm-pagination';
import { conditionsMapping } from '@app/common/conditions-mapping';

export interface FindLendingByOptions {
  userId?: string;
  status?: LendingStatusEnum;
}

@Injectable()
export class LendingPgRepository {
  public constructor(
    @InjectRepository(Lending) private readonly repository: Repository<Lending>,
  ) {}

  public async save(lending: Lending): Promise<void> {
    await this.repository.save(lending);
  }

  public async findBy(
    options?: FindLendingByOptions,
    pagination?: PaginationDto,
  ): Promise<{ items: Lending[]; total: number }> {
    const [items, total] = await this.repository.findAndCount({
      where: conditionsMapping({
        status: options.status,
        account: options.userId ? { userId: options.userId } : undefined,
      }),
      ...typeormPagination(pagination),
    });

    return { items, total };
  }

  public async findInProgress(): Promise<Lending[]> {
    return this.repository.find({
      where: {
        status: LendingStatusEnum.IN_PROGRESS,
      },
    });
  }

  public async findByUserId(userId: string): Promise<Lending[]> {
    return this.repository.find({
      where: {
        account: {
          userId,
        },
      },
      relations: ['account'],
    });
  }

  public async getById(id: string): Promise<Lending> {
    const found = await this.repository.findOne({
      where: {
        id,
      },
    });

    if (!found) {
      throw new BaseException({
        statusCode: ErrorCodeEnum.NOT_FOUND,
        message: 'Lending not found',
      });
    }

    return found;
  }
}
