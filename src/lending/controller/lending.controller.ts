import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { LendingResponse } from '@app/lending/controller/lending.response';
import { LendingCreateCommand } from '@app/lending/command/create/lending-create.command';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { LendingListQuery } from '@app/lending/query/list/lending-list.query';
import { AuthGuard } from '@app/jwt/guard/auth-guard';
import { Lending } from '@app/lending/dao/entity/lending';
import { ReturnLendingCommand } from '@app/lending/command/return/return-lending.command';
import { AuthPayload } from '@app/jwt/decorator/auth-payload';
import { TokenPayload } from '@app/jwt/interfaces/token-payload.interface';

@Controller()
@ApiTags('Lending')
export class LendingController {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @Post()
  @ApiCreatedResponse({
    type: LendingResponse,
  })
  public async create(
    @Body() command: LendingCreateCommand,
  ): Promise<LendingResponse> {
    return new LendingResponse(await this.commandBus.execute(command));
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @Get(':userId')
  @ApiOkResponse({
    type: LendingResponse,
    isArray: true,
  })
  public async list(
    @Param('userId') userId: string,
  ): Promise<LendingResponse[]> {
    const lendingPrograms = await this.queryBus.execute<
      LendingListQuery,
      Lending[]
    >(new LendingListQuery(userId));

    return lendingPrograms.map((lending) => new LendingResponse(lending));
  }

  @Patch(':lendingId/return')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOkResponse({
    type: LendingResponse,
  })
  public async return(
    @Param('lendingId') lendingId: string,
    @AuthPayload() authPayload: TokenPayload,
  ): Promise<LendingResponse> {
    return new LendingResponse(
      await this.commandBus.execute(
        new ReturnLendingCommand(lendingId, authPayload.userId),
      ),
    );
  }
}
