import { AccountResponse } from '@app/account/controller/account.response';
import { LendingStatusEnum } from '@app/lending/dao/lending-status.enum';
import { Lending } from '@app/lending/dao/entity/lending';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class LendingResponse {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty({
    type: AccountResponse,
  })
  public readonly account: AccountResponse;

  @ApiProperty({
    type: 'string',
  })
  @Type(() => String)
  public readonly amount: bigint;

  @ApiProperty({
    type: 'string',
  })
  @Type(() => String)
  public readonly interestsAmount: bigint;

  @ApiProperty({
    type: Date,
    nullable: true,
  })
  public readonly lastInterestsAt: Date | null;

  @ApiProperty({
    enum: {
      IN_PROGRESS: LendingStatusEnum.IN_PROGRESS,
      CLOSED: LendingStatusEnum.CLOSED,
    },
    type: 'enum',
  })
  public readonly status: LendingStatusEnum;

  @ApiProperty()
  public readonly endDate: Date;

  @ApiProperty()
  public readonly createdAt: Date;

  constructor(lending: Lending) {
    this.id = lending.id;
    this.account = new AccountResponse(lending.account);
    this.amount = lending.amount;
    this.interestsAmount = lending.interestsAmount;
    this.lastInterestsAt = lending.lastInterestsAt;
    this.status = lending.status;
    this.endDate = lending.endDate;
    this.createdAt = lending.createdAt;
  }
}
