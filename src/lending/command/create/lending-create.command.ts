import { ICommand } from '@nestjs/cqrs';
import { IsDate, IsDefined, IsUUID } from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class LendingCreateCommand implements ICommand {
  @ApiProperty()
  @IsDefined()
  @IsUUID('4')
  public readonly userId: string;

  @ApiProperty({
    type: 'string',
    example: '1000000000000000000',
  })
  @Type(() => BigInt)
  @Transform(({ value }) => BigInt(value), { toClassOnly: true })
  @IsDefined()
  public readonly amount: bigint;

  @ApiProperty()
  @IsDefined()
  @IsDate()
  @Type(() => Date)
  public readonly endDate: Date;
}
