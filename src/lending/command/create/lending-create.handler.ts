import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { LendingCreateCommand } from '@app/lending/command/create/lending-create.command';
import { Lending } from '@app/lending/dao/entity/lending';
import { AccountCreateHandler } from '@app/account/command/create/account-create.handler';
import { LendingPgRepository } from '@app/lending/repository/lending-pg.repository';
import { AccountTypeEnum } from '@app/account/dao/account-type.enum';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { TransferHandler } from '@app/transfer/command/transfer.handler';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';

@CommandHandler(LendingCreateCommand)
export class LendingCreateHandler
  implements ICommandHandler<LendingCreateCommand, Lending>
{
  private readonly lendingInProgressLimit: number = 1;

  constructor(
    private readonly accountCreateHandler: AccountCreateHandler,
    private readonly accountPgRepository: AccountPgRepository,
    private readonly lendingPgRepository: LendingPgRepository,
    private readonly transferHandler: TransferHandler,
  ) {}

  public async execute(command: LendingCreateCommand): Promise<Lending> {
    const userLendingList = await this.lendingPgRepository.findByUserId(
      command.userId,
    );
    const inProgressLending = userLendingList.filter(
      (lending) => lending.inProgress,
    );

    if (this.lendingInProgressLimit <= inProgressLending.length) {
      throw new BaseException({
        message: `Cannot create more than ${this.lendingInProgressLimit} active lending`,
        statusCode: ErrorCodeEnum.LENDING_OUT_OF_LIMITS,
      });
    }

    const [mainAccount, lendingAccount] = await Promise.all([
      this.accountPgRepository.getMainByUserId(command.userId),
      this.accountCreateHandler.execute({
        type: AccountTypeEnum.LENDING,
        userId: command.userId,
      }),
    ]);
    const lending = Lending.create(
      lendingAccount,
      command.amount,
      command.endDate,
    );

    await this.lendingPgRepository.save(lending);
    await this.transferHandler.execute({
      amount: command.amount,
      recipient: lendingAccount.address,
      sender: mainAccount.userId,
    });

    return lending;
  }
}
