import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { LendingAccrueCommand } from '@app/lending/command/accrue/lending-accrue.command';
import { Lending } from '@app/lending/dao/entity/lending';
import { Injectable } from '@nestjs/common';
import { LendingPgRepository } from '@app/lending/repository/lending-pg.repository';
import { DispenseAccountService } from '@app/dispenser/service/dispense-account.service';
import { LendingAccruePercentService } from '@app/lending/service/lending-accrue-percent.service';

@Injectable()
@CommandHandler(LendingAccrueCommand)
export class LendingAccrueHandler
  implements ICommandHandler<LendingAccrueCommand, Lending[]>
{
  constructor(
    private readonly lendingPgRepository: LendingPgRepository,
    private readonly lendingDispenseAccountService: DispenseAccountService,
    private readonly lendingAccruePercentService: LendingAccruePercentService,
  ) {}

  public async execute(): Promise<Lending[]> {
    const lendingPrograms = await this.lendingPgRepository.findInProgress();
    const accrualPrograms = [] as Lending[];

    for (const lending of lendingPrograms) {
      try {
        const accrualAmount =
          await this.lendingAccruePercentService.calcOneTimeAccrual(lending);

        if (accrualAmount > BigInt(0)) {
          await this.lendingDispenseAccountService.transfer(
            lending.account,
            accrualAmount,
          );
        }

        const accrualProgram = lending.accrueInterest(accrualAmount);
        await this.lendingPgRepository.save(accrualProgram);
        accrualPrograms.push(accrualProgram);
      } catch (e) {
        console.error(lending, e);
      }
    }

    return accrualPrograms;
  }
}
