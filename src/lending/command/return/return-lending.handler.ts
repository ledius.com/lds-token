import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { ReturnLendingCommand } from '@app/lending/command/return/return-lending.command';
import { Lending } from '@app/lending/dao/entity/lending';
import { LendingPgRepository } from '@app/lending/repository/lending-pg.repository';
import { ContractFactoryService } from '@app/smart-chain/services/contract-factory.service';
import { TokenSymbolEnum } from '@app/smart-chain/token-symbol.enum';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { DispenseBnbService } from '@app/dispenser/service/dispense-bnb.service';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';

@CommandHandler(ReturnLendingCommand)
export class ReturnLendingHandler
  implements ICommandHandler<ReturnLendingCommand, Lending>
{
  constructor(
    private readonly lendingPgRepository: LendingPgRepository,
    private readonly contractFactoryService: ContractFactoryService,
    private readonly accountPgRepository: AccountPgRepository,
    private readonly dispenseBnbService: DispenseBnbService,
  ) {}

  public async execute(command: ReturnLendingCommand): Promise<Lending> {
    const lending = await this.lendingPgRepository.getById(command.lendingId);

    if (lending.account.userId !== command.userId) {
      throw new BaseException({
        statusCode: ErrorCodeEnum.LENDING_OUT_OF_WORKFLOW,
        message: 'You cannot closed lending if you not owner',
      });
    }

    const contract = this.contractFactoryService.getContract(
      TokenSymbolEnum.LDS,
    );
    const recipient = await this.accountPgRepository.getMainByUserId(
      lending.account.userId,
    );

    await this.dispenseBnbService.dispenseFee(lending.account);

    return (await lending.return(contract, recipient)).saveTo(
      this.lendingPgRepository,
    );
  }
}
