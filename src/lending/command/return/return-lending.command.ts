import { ICommand } from '@nestjs/cqrs';
import { IsDefined, IsUUID } from 'class-validator';

export class ReturnLendingCommand implements ICommand {
  @IsDefined()
  @IsUUID('4')
  public readonly lendingId: string;

  @IsDefined()
  @IsUUID('4')
  public readonly userId: string;

  constructor(lendingId: string, userId: string) {
    this.lendingId = lendingId;
    this.userId = userId;
  }
}
