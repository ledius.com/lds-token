import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateLendingTable1641730902242 implements MigrationInterface {
  public readonly name = 'CreateLendingTable1641730902242';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "public"."lending_programs_status_enum" AS ENUM('0', '1')`,
    );
    await queryRunner.query(
      `CREATE TABLE "lending_programs" ("id" uuid NOT NULL, "amount" numeric(36) NOT NULL, "interests_amount" numeric(36) NOT NULL, "status" "public"."lending_programs_status_enum" NOT NULL DEFAULT '0', "end_date" TIMESTAMP WITH TIME ZONE NOT NULL, "last_interests_at" TIMESTAMP WITH TIME ZONE, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "account_id" uuid, CONSTRAINT "PK_da2d00800e0fb9e5af7b84048a4" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "lending_programs" ADD CONSTRAINT "FK_247e7310fd99280059abf5ef06a" FOREIGN KEY ("account_id") REFERENCES "accounts"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lending_programs" DROP CONSTRAINT "FK_247e7310fd99280059abf5ef06a"`,
    );
    await queryRunner.query(`DROP TABLE "lending_programs"`);
    await queryRunner.query(
      `DROP TYPE "public"."lending_programs_status_enum"`,
    );
  }
}
