import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddReturnedStatus1646503974044 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TYPE "public"."lending_programs_status_enum" ADD VALUE '2'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    // dont rollback enum
  }
}
