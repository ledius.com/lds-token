export enum LendingStatusEnum {
  IN_PROGRESS,
  CLOSED,
  RETURNED,
}
