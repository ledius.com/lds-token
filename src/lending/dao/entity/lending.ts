import { Account } from '@app/account/dao/entity/account';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';
import { BigIntColumn } from '@app/common/typeorm/bigint.column';
import { v4 } from 'uuid';
import { LendingStatusEnum } from '@app/lending/dao/lending-status.enum';
import { isAfter } from 'date-fns';
import { BEP20TokenInterface } from '@app/smart-chain/BEP20-token.interface';
import { BaseException } from '@app/common/base-exception';
import { ErrorCodeEnum } from '@app/common/error-code.enum';

const tableName = 'lending_programs';

@Entity(tableName)
export class Lending {
  @PrimaryColumn('uuid')
  public readonly id: string;

  @ManyToOne(() => Account, {
    eager: true,
  })
  public readonly account: Account;

  @BigIntColumn()
  public readonly amount: bigint;

  @BigIntColumn()
  public readonly interestsAmount: bigint;

  @Column({
    type: 'enum',
    enum: LendingStatusEnum,
    default: LendingStatusEnum.IN_PROGRESS,
  })
  public readonly status: LendingStatusEnum;

  @Column('timestamp with time zone')
  public readonly endDate: Date;

  @Column('timestamp with time zone', {
    nullable: true,
  })
  public readonly lastInterestsAt: Date | null;

  @CreateDateColumn()
  public readonly createdAt: Date;

  private constructor(
    id: string,
    account: Account,
    amount: bigint,
    interestsAmount: bigint,
    createdAt: Date,
    endDate: Date,
    status: LendingStatusEnum,
    lastInterestsAt: Date | null,
  ) {
    this.id = id;
    this.account = account;
    this.amount = amount;
    this.interestsAmount = interestsAmount;
    this.status = status;
    this.endDate = endDate;
    this.lastInterestsAt = lastInterestsAt;
    this.createdAt = createdAt;
  }

  public static create(account: Account, amount: bigint, endDate: Date) {
    return new Lending(
      v4(),
      account,
      amount,
      0n,
      new Date(),
      endDate,
      LendingStatusEnum.IN_PROGRESS,
      null,
    );
  }

  public close(): Lending {
    return new Lending(
      this.id,
      this.account,
      this.amount,
      this.interestsAmount,
      this.createdAt,
      this.endDate,
      LendingStatusEnum.CLOSED,
      this.lastInterestsAt,
    );
  }

  public accrueInterest(
    interest: bigint,
    interestDate: Date = new Date(),
  ): Lending {
    const shouldBeClose = isAfter(interestDate, this.endDate);
    const status = shouldBeClose
      ? LendingStatusEnum.CLOSED
      : LendingStatusEnum.IN_PROGRESS;
    const updatedInterestDateAt: Date =
      interest > BigInt(0) ? interestDate : this.lastInterestsAt;

    return new Lending(
      this.id,
      this.account,
      this.amount,
      this.interestsAmount + interest,
      this.createdAt,
      this.endDate,
      status,
      updatedInterestDateAt,
    );
  }

  public async return(
    contract: BEP20TokenInterface,
    recipient: Account,
  ): Promise<Lending> {
    if (!this.isClosed) {
      throw new BaseException({
        message: 'Only closed lending can be returned',
        statusCode: ErrorCodeEnum.LENDING_OUT_OF_WORKFLOW,
      });
    }

    await contract.transfer(this.account, recipient, this.summaryAmount);

    return new Lending(
      this.id,
      this.account,
      this.amount,
      this.interestsAmount,
      this.createdAt,
      this.endDate,
      LendingStatusEnum.RETURNED,
      this.lastInterestsAt,
    );
  }

  public async saveTo(repository: {
    save(lending: Lending): Promise<void>;
  }): Promise<Lending> {
    await repository.save(this);
    return this;
  }

  public get isClosed(): boolean {
    return this.status === LendingStatusEnum.CLOSED;
  }

  public get inProgress(): boolean {
    return this.status === LendingStatusEnum.IN_PROGRESS;
  }

  public get summaryAmount(): bigint {
    return this.amount + this.interestsAmount;
  }
}
