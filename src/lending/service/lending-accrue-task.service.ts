import { ConsoleLogger, Injectable, LoggerService } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Cron, CronExpression } from '@nestjs/schedule';
import { LendingAccrueCommand } from '@app/lending/command/accrue/lending-accrue.command';
import { Lending } from '@app/lending/dao/entity/lending';

@Injectable()
export class LendingAccrueTaskService {
  private readonly logger: LoggerService = new ConsoleLogger(
    LendingAccrueTaskService.name,
  );

  constructor(private readonly commandBus: CommandBus) {}

  @Cron(CronExpression.EVERY_10_MINUTES)
  public async accrue(): Promise<void> {
    this.logger.log('accrue routine start');
    const lendingPrograms = await this.commandBus.execute<
      LendingAccrueCommand,
      Lending[]
    >(new LendingAccrueCommand());

    lendingPrograms.forEach((lending) => {
      this.logger.log(
        `accrue to ${lending.account.address} interest amount ${lending.interestsAmount}`,
      );
    });
  }
}
