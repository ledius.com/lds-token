import { Lending } from '@app/lending/dao/entity/lending';
import { isAfter, subSeconds } from 'date-fns';
import { ConsoleLogger, Injectable } from '@nestjs/common';

@Injectable()
export class LendingAccruePercentService {
  private readonly logger: ConsoleLogger = new ConsoleLogger(
    LendingAccruePercentService.name,
  );

  private readonly percent: number = 0.001;
  private readonly interestInterval: number = 86400;

  public async calcOneTimeAccrual(lending: Lending): Promise<bigint> {
    this.logger.log(
      `[${lending.id}] calcOneTimeAccrual is closed - ${lending.isClosed}`,
    );
    const endDatePast = isAfter(new Date(), lending.endDate);
    const interestDatePast = isAfter(
      subSeconds(new Date(), this.interestInterval),
      lending.lastInterestsAt ?? lending.createdAt,
    );

    if (endDatePast || lending.isClosed) {
      this.logger.log(
        `[${lending.id}] end date past [${endDatePast}] or lending is closed [${lending}]`,
      );
      return BigInt(0);
    }

    if (!interestDatePast) {
      this.logger.log(
        `[${lending.id}] not interestDatePast [${interestDatePast}]`,
      );
      return BigInt(0);
    }

    const amountToAccrual = this.amountToAccrualCalculation(
      lending.amount,
      this.percent,
    );

    this.logger.log(`[${lending.id}] amount to accrual = ${amountToAccrual}`);

    return amountToAccrual;
  }

  public amountToAccrualCalculation(amount: bigint, percent: number): bigint {
    const percentInBN = BigInt(percent * 10000);
    const summary = amount * percentInBN;
    const value = summary / BigInt(10000);

    return value;
  }
}
