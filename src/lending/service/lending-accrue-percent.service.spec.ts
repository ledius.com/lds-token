import { LendingAccruePercentService } from '@app/lending/service/lending-accrue-percent.service';

describe('lending accrue percent service', () => {
  let lendingAccruePercentService: LendingAccruePercentService;

  beforeEach(() => {
    lendingAccruePercentService = new LendingAccruePercentService();
  });

  describe('amountToAccrualCalculation', () => {
    describe('calc accrue amount for 1 - 100% percents', () => {
      it('accrue for 1%', () => {
        const initAmount = BigInt(10_000);
        const percent = 0.01;
        const accrualAmount =
          lendingAccruePercentService.amountToAccrualCalculation(
            initAmount,
            percent,
          );

        expect(accrualAmount).toStrictEqual(BigInt(100));
      });
    });

    describe('calc accrue amount for 0.1% - 1% percents', () => {
      it('accrue for 0.1%', () => {
        const initAmount = BigInt(10_000);
        const percent = 0.001;
        const accrualAmount =
          lendingAccruePercentService.amountToAccrualCalculation(
            initAmount,
            percent,
          );

        expect(accrualAmount).toStrictEqual(BigInt(10));
      });
    });

    describe('calc accrue amount for 0.01% - 0.1% percents', () => {
      it('accrue for 0.01%', () => {
        const initAmount = BigInt(10_000);
        const percent = 0.0001;
        const accrualAmount =
          lendingAccruePercentService.amountToAccrualCalculation(
            initAmount,
            percent,
          );

        expect(accrualAmount).toStrictEqual(BigInt(1));
      });
    });

    describe('calc amount for 11 tokens in 10 ** 18. Reproduce bug', () => {
      it('accrue for 0.01%', () => {
        const initAmount = BigInt(11_000_000_000_000_000_000);
        const percent = 0.001;
        const accrualAmount =
          lendingAccruePercentService.amountToAccrualCalculation(
            initAmount,
            percent,
          );

        expect(accrualAmount).toStrictEqual(BigInt(11_000_000_000_000_000));
      });
    });
  });
});
