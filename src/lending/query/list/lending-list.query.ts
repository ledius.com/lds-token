import { IQuery } from '@nestjs/cqrs';
import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class LendingListQuery implements IQuery {
  @ApiProperty()
  @IsUUID('4')
  public readonly userId: string;

  constructor(userId: string) {
    this.userId = userId;
  }
}
