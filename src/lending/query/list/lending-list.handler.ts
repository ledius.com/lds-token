import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { LendingListQuery } from '@app/lending/query/list/lending-list.query';
import { Lending } from '@app/lending/dao/entity/lending';
import { LendingPgRepository } from '@app/lending/repository/lending-pg.repository';

@QueryHandler(LendingListQuery)
export class LendingListHandler
  implements IQueryHandler<LendingListQuery, Lending[]>
{
  constructor(private readonly lendingPgRepository: LendingPgRepository) {}

  public async execute(query: LendingListQuery): Promise<Lending[]> {
    return this.lendingPgRepository.findByUserId(query.userId);
  }
}
