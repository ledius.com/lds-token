import { Module } from '@nestjs/common';
import { HttpAuthModule } from '@app/http-auth/http-auth.module';
import { AccountModule } from '@app/account/account.module';
import { UsersController } from '@app/users/controller/users.controller';
import { BalanceModule } from '@app/balance/balance.module';

@Module({
  imports: [HttpAuthModule, AccountModule, BalanceModule],
  controllers: [UsersController],
})
export class UsersModule {}
