import { AccountResponse } from '@app/account/controller/account.response';
import { BalanceResponse } from '@app/balance/controller/balance.response';
import { ApiProperty } from '@nestjs/swagger';
import { AuthUserInterface } from '@app/http-auth/service/auth-client.service';
import { Account } from '@app/account/dao/entity/account';
import { AssetBalance } from '@app/balance/query/account-balance/account-balance.handler';

export class UserResponse {
  @ApiProperty({
    type: 'string',
  })
  public readonly userId: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly email: string;

  @ApiProperty({
    type: 'string',
  })
  public readonly name: string;

  @ApiProperty({
    type: 'string',
    isArray: true,
  })
  public readonly roles: string[];

  @ApiProperty({
    type: 'string',
  })
  public readonly picture: string;

  @ApiProperty({
    type: AccountResponse,
  })
  public readonly account: AccountResponse;

  @ApiProperty({
    type: BalanceResponse,
    isArray: true,
  })
  public readonly balances: BalanceResponse[];

  constructor(
    user: AuthUserInterface,
    account: Account,
    balances: AssetBalance[],
  ) {
    this.userId = user.id;
    this.email = user.email;
    this.name = user.name;
    this.picture = user.picture;
    this.roles = user.roles;
    this.account = new AccountResponse(account);
    this.balances = balances.map((balance) => new BalanceResponse(balance));
  }
}
