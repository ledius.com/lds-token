import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { UserResponse } from '@app/users/controller/user.response';
import {
  AccountBalanceHandler,
  AssetBalance,
} from '@app/balance/query/account-balance/account-balance.handler';
import { AuthClientService } from '@app/http-auth/service/auth-client.service';
import { Account } from '@app/account/dao/entity/account';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { AccountTypeEnum } from '@app/account/dao/account-type.enum';

type AccountBalance = { account: Account; balances: AssetBalance[] };

@Controller()
@ApiTags('Users')
export class UsersController {
  constructor(
    private readonly accountBalanceHandler: AccountBalanceHandler,
    private readonly authClientService: AuthClientService,
    private readonly accountPgRepository: AccountPgRepository,
  ) {}

  @Get()
  @ApiOkResponse({
    type: UserResponse,
    isArray: true,
  })
  public async getUsers(): Promise<UserResponse[]> {
    const accounts = await this.accountPgRepository.findByType(
      AccountTypeEnum.MAIN,
    );
    const authUsers = await this.authClientService.getUsersByIds(
      accounts.map((account) => account.userId),
    );
    const balancesWithAccount: AccountBalance[] = await Promise.all(
      accounts.map(async (account) => {
        return {
          account,
          balances: await this.accountBalanceHandler.execute(account),
        };
      }),
    );

    return accounts
      .map((account) => {
        const authUser = authUsers.find((user) => user.id === account.userId);
        const accountBalances = balancesWithAccount.filter(
          (balance) => balance.account.id === account.id,
        );

        if (!authUser) {
          return false;
        }

        return new UserResponse(
          authUser,
          account,
          accountBalances.flatMap((a) => a.balances),
        );
      })
      .filter(Boolean) as UserResponse[];
  }

  @Get(':userId')
  @ApiOkResponse({
    type: UserResponse,
  })
  public async getUser(@Param('userId') userId: string): Promise<UserResponse> {
    const account = await this.accountPgRepository.getMainByUserId(userId);
    const authUser = await this.authClientService.getUserById(userId);
    const balances: AssetBalance[] = await this.accountBalanceHandler.execute(
      account,
    );

    return new UserResponse(authUser, account, balances);
  }
}
