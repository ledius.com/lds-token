import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { ApplyTemplateCommand } from '@app/offer-template/cqrs/apply-template.command';
import { Offer } from '@app/offer/dao/entity/offer';
import { OfferPgRepository } from '@app/offer/repository/offer-pg.repository';
import { AccountPgRepository } from '@app/account/repository/account-pg.repository';
import { In } from 'typeorm';
import { OfferStatusEnum } from '@app/offer/dao/offer-status.enum';
import { CreateOfferHandler } from '@app/offer/command/create/create-offer.handler';

@CommandHandler(ApplyTemplateCommand)
export class ApplyTemplateHandler
  implements ICommandHandler<ApplyTemplateCommand, Offer>
{
  public constructor(
    private readonly offerRepository: OfferPgRepository,
    private readonly accountRepository: AccountPgRepository,
    private readonly createOfferHandler: CreateOfferHandler,
  ) {}

  public async execute(command: ApplyTemplateCommand): Promise<Offer> {
    const [offers, count] = await this.offerRepository.findBy({
      type: command.type,
      owner: {
        userId: command.ownerId,
      },
      amount: command.amount,
      status: In([OfferStatusEnum.OPENED, OfferStatusEnum.RESERVED]),
    });

    if (count) {
      return offers[0];
    }

    return this.createOfferHandler.execute(command);
  }
}
