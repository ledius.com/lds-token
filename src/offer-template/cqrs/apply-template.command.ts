import { ICommand } from '@nestjs/cqrs';
import { OfferTypeEnum } from '@app/offer/dao/offer-type.enum';
import { CreateOfferCommand } from '@app/offer/command/create/create-offer.command';

export class ApplyTemplateCommand implements ICommand, CreateOfferCommand {
  public readonly amount: bigint;
  public readonly type: OfferTypeEnum;
  public readonly ownerId: string;
  public readonly price: number;
  public readonly publishAt: Date;
}
