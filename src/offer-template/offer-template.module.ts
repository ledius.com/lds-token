import { Module } from '@nestjs/common';
import { AccountModule } from '@app/account/account.module';
import { OfferModule } from '@app/offer/offer.module';
import { ApplyTemplateHandler } from '@app/offer-template/cqrs/apply-template.handler';
import { CqrsModule } from '@nestjs/cqrs';
import { ApplyTemplateService } from '@app/offer-template/service/apply-template.service';
import { DispenserModule } from '@app/dispenser/dispenser.module';

@Module({
  imports: [AccountModule, OfferModule, DispenserModule, CqrsModule],
  providers: [ApplyTemplateHandler, ApplyTemplateService],
})
export class OfferTemplateModule {}
