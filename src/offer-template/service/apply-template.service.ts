import { Injectable } from '@nestjs/common';
import { ApplyTemplateHandler } from '@app/offer-template/cqrs/apply-template.handler';
import { DispenseAccountService } from '@app/dispenser/service/dispense-account.service';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ApplyTemplateCommand } from '@app/offer-template/cqrs/apply-template.command';
import { OfferTypeEnum } from '@app/offer/dao/offer-type.enum';

@Injectable()
export class ApplyTemplateService {
  constructor(
    private readonly applyTemplateHandler: ApplyTemplateHandler,
    private readonly dispenseAccountService: DispenseAccountService,
  ) {}

  public async getTemplatesCommand(): Promise<ApplyTemplateCommand[]> {
    const dispenserAccount =
      await this.dispenseAccountService.ensureExistence();

    const toScale = (amount: number | bigint) =>
      BigInt(amount) * BigInt(10 ** 18);

    return [
      {
        ownerId: dispenserAccount.userId,
        amount: toScale(350),
        type: OfferTypeEnum.SELL,
        price: 3499 * 100,
        publishAt: new Date(),
      },
      {
        ownerId: dispenserAccount.userId,
        amount: toScale(700),
        type: OfferTypeEnum.SELL,
        price: 6999 * 100,
        publishAt: new Date(),
      },
      {
        ownerId: dispenserAccount.userId,
        amount: toScale(1400),
        type: OfferTypeEnum.SELL,
        price: 13999 * 100,
        publishAt: new Date(),
      },
    ];
  }

  @Cron(CronExpression.EVERY_MINUTE)
  public async apply(): Promise<void> {
    const commands = await this.getTemplatesCommand();
    for (const command of commands) {
      await this.applyTemplateHandler.execute(command);
    }
  }
}
