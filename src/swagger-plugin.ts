import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AccountModule } from '@app/account/account.module';
import { BalanceModule } from '@app/balance/balance.module';
import { BankDetailsModule } from '@app/bank-details/bank-details.module';
import { OfferModule } from '@app/offer/offer.module';
import { TransferModule } from '@app/transfer/transfer.module';
import { LendingModule } from '@app/lending/lending.module';
import { DispenserModule } from '@app/dispenser/dispenser.module';
import { HistoryModule } from '@app/history/history.module';
import { UsersModule } from '@app/users/users.module';

export function swaggerPlugin(app: INestApplication): void {
  const config = new DocumentBuilder()
    .setTitle('LDS Token Api')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config, {
    include: [
      AccountModule,
      BalanceModule,
      BankDetailsModule,
      OfferModule,
      TransferModule,
      LendingModule,
      DispenserModule,
      HistoryModule,
      UsersModule,
    ],
  });
  SwaggerModule.setup('/api/ledius-token/docs', app, document, {
    customSiteTitle: 'Ledius Token',
  });
}
