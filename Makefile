setup: init
init: docker-down-clear docker-build docker-up app-wait-db app-migration app-stop app-up
check: lint test-full
test-full: setup init test-unit test-e2e

test-unit:
	npm run test

test-e2e:
	docker-compose run --rm ledius-token npm run test:e2e

lint:
	npm run lint

docker-down-clear:
	docker-compose down --remove-orphans

docker-up:
	docker-compose up -d

docker-stop:
	docker-compose down

app-stop:
	docker-compose stop ledius-token

app-up:
	docker-compose up ledius-token

app-fixtures:
	docker-compose run --rm ledius-token npx nestjs-command fixtures:load

app-wait-db:
	sleep 5

app-migration:
	docker-compose run --rm ledius-token npx typeorm migration:run

docker-build:
	docker-compose build

migration-gen:
	docker-compose run --rm ledius-token npx typeorm migration:generate -n $(n) -d ./src/$(m)/dao/migrations && sudo chown -R ${USER}:${USER} src/$(m)
